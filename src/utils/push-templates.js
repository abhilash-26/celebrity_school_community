const pushTemplates = {
  "what_are_you_missingout-iOS": (name, number) => `
{
"APNSMessage": {
"aps": {
"alert": ""
}
},
"GCMMessage": {
"data":{"notificationId":"t672y7jinj",
"courseId":"{{Attributes.AblumID}}",
"category":"course",
"by":"{{Attributes.artistName}}",
"mymediavideo":"{{Attributes.artistImage}}"},
"notification":
{
"body": "Hi {{Attributes.userName}}, You are missing a lot from {{Attributes.artistName}} Here it is... 1.How to network 2.How to enhance your {{Attributes.courseName}} skills  3.{{Attributes.artistName}}’s tips, techniques, and secrets 4.Perform with {{Attributes.artistName}} 5.Get Advice from industry expert For Any Queries Feel Free to Call ${name} - ${number}","title":"What are you missing out?","subtitle":"{{Attributes.artistName}}"
}
},
"ADMMessage": {
"data": {
"message": ""
}
},
"BaiduMessage": {
"title": "",
"description": ""
}
}
`,
  welcome_push_notification: (name, number) => `
  {
	"APNSMessage": {
		"aps": {
			"alert": ""
		}
	},
	"GCMMessage": {
		"data": {
			"notificationId": "982uuwh89u82",
			"title": "Welcome",
			"category": "course",
			"courseId": "{{Attributes.AblumID}}",
			"image": "{{Attributes.artistImage}}",
			"message": "Hi {{Attributes.userName}}, Join {{Attributes.artistName}}’s {{Attributes.courseName}} and learn all the Techniques, Secrets & Tips. For Any Queries Feel Free to Call ${name} - ${number}",
			"by": "{{Attributes.artistName}}"
		}
	},
	"ADMMessage": {
		"data": {
			"message": ""
		}
	},
	"BaiduMessage": {
		"title": "",
		"description": ""
	}
}
    `,
  celebrity_is_waiting: (name, number) => `
{
  "APNSMessage": {
    "aps": {
      "alert": ""
    }
  },
  "GCMMessage": {
    "data": {
      "notificationId": "6tyhtg",
      "title": "{{Attributes.artistName}} is waiting",
      "category": "course",
      "message": "Hi {{Attributes.userName}}, {{Attributes.artistName}} is waiting for you Join {{Attributes.artistName}}’s {{Attributes.courseName}} now and achieve your {{Attributes.courseName}} Goal? {{Attributes.artistName}} will teach you all  {{Attributes.courseName}} techniques and secrets to help you build the foundation for a lifetime For Any Queries Feel Free to Call ${name} - ${number}",
      "courseId": "{{Attributes.AblumID}}",
      "by": "{{Attributes.artistName}}",
      "image": "{{Attributes.artistImage}}"
    }
  },
  "ADMMessage": {
    "data": {
      "message": ""
    }
  },
  "BaiduMessage": {
    "title": "",
    "description": ""
  }
}
`,
  discount_push_notification: (name, number) => `
{
  "APNSMessage": {
    "aps": {
      "alert": ""
    }
  },
  "GCMMessage": {
    "data": {
      "notificationId": "982uuwh89u82",
      "title": "Discount",
      "category": "course",
      "courseId": "{{Attributes.AblumID}}",
      "image": "{{Attributes.artistImage}}",
      "message": "Hi {{Attributes.userName}}, We miss you Get flat 10% off on {{Attributes.artistName}}’s {{Attributes.courseName}} just for you For Any Queries Feel Free to Call ${name} - ${number}",
      "by": "{{Attributes.artistName}}"
    }
  },
  "ADMMessage": {
    "data": {
      "message": ""
    }
  },
  "BaiduMessage": {
    "title": "",
    "description": ""
  }
}
`,
  "celebrity_is_waiting-iOS": (name, number) => `
  {
  "APNSMessage": {
    "aps": {
      "alert": ""
    }
  },
  "GCMMessage": {
    "data": {
      "notificationId": "6tyhtg",
      "category": "course",
      "mymediavideo": "{{Attributes.artistImage}}",
      "courseId": "{{Attributes.AblumID}}"
    },
    "notification": {
      "body": "Hi {{Attributes.userName}}, {{Attributes.artistName}} is waiting for you Join {{Attributes.artistName}}’s {{Attributes.courseName}} now and achieve your {{Attributes.courseName}} Goal? {{Attributes.artistName}} will teach you all  {{Attributes.courseName}} techniques and secrets to help you build the foundation for a lifetime For Any Queries Feel Free to Call ${name} - ${number}",
      "title": "{{Attributes.artistName}} is waiting",
      "subtitle": "{{Attributes.artistName}}"
    }
  },
  "ADMMessage": {
    "data": {
      "message": ""
    }
  },
  "BaiduMessage": {
    "title": "",
    "description": ""
  }
}`,
  what_are_you_missingout: (name, number) => `
{
  "APNSMessage": {
    "aps": {
      "alert": ""
    }
  },
  "GCMMessage": {
    "data": {
      "notificationId": "t672y7jinj",
      "title": "What are you missing out?",
      "message": "Hi {{Attributes.userName}}, You are missing a lot from {{Attributes.artistName}} Here it is... 1.How to network 2.How to enhance your {{Attributes.courseName}} skills  3.{{Attributes.artistName}}’s tips, techniques, and secrets 4.Perform with {{Attributes.artistName}} 5.Get Advice from industry expert For Any Queries Feel Free to Call ${name} - ${number}",
      "courseId": "{{Attributes.AblumID}}",
      "category": "course",
      "by": "{{Attributes.artistName}}",
      "image": "{{Attributes.artistImage}}"
    }
  },
  "ADMMessage": {
    "data": {
      "message": ""
    }
  },
  "BaiduMessage": {
    "title": "",
    "description": ""
  }
}
`,
  "discount_push_notification-iOS": (name, number) => `
{
  "APNSMessage": {
    "aps": {
      "alert": ""
    }
  },
  "GCMMessage": {
    "data": {
      "notificationId": "982uuwh89u82",
      "category": "course",
      "courseId": "{{Attributes.AblumID}}",
      "mymediavideo": "{{Attributes.artistImage}}",
      "by": "{{Attributes.artistName}}"
    },
    "notification": {
      "body": "Hi {{Attributes.userName}}, We miss you Get flat 10% off on {{Attributes.artistName}}’s {{Attributes.courseName}} just for you For Any Queries Feel Free to Call ${name} - ${number}",
      "title": "Discount",
      "subtitle": "{{Attributes.artistName}}"
    }
  },
  "ADMMessage": {
    "data": {
      "message": ""
    }
  },
  "BaiduMessage": {
    "title": "",
    "description": ""
  }
}
`,
  "welcome_push_notification-iOS": (name, number) => `
{
  "APNSMessage": {
    "aps": {
      "alert": ""
    }
  },
  "GCMMessage": {
    "data": {
      "notificationId": "982uuwh89u82",
      "category": "course",
      "courseId": "{{Attributes.AblumID}}",
      "mymediavideo": "{{Attributes.artistImage}}"
    },
    "notification": {
      "body": "Hi {{Attributes.userName}}, Join {{Attributes.artistName}}’s {{Attributes.courseName}} and learn all the Techniques, Secrets & Tips. For Any Queries Feel Free to Call ${name} - ${number}",
      "title": "Welcome",
      "subtitle": "{{Attributes.artistName}}"
    }
  },
  "ADMMessage": {
    "data": {
      "message": ""
    }
  },
  "BaiduMessage": {
    "title": "",
    "description": ""
  }
}
`,
};

module.exports = pushTemplates;
