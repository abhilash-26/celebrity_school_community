const mongoose = require("mongoose");

const batch = mongoose.Schema(
  {
    batchName: {
      type: String,
    },

    startAt: {
      type: Date,
      // required: true,
    },
    usersEnrolled: [],
    endAt: {
      type: Date,
      // required: true,
    },
  },
  { timestamp: true }
);

module.exports = mongoose.model("Batch", batch);
