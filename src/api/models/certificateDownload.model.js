const mongoose = require("mongoose");

const certificateDownload = new mongoose.Schema(
  {
    userId: {
      type: Number,
      required: true,
    },
    userEmail: {
      type: String,
      required: true,
    },
    courseId: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("CertificateDownload", certificateDownload);
