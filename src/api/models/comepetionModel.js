const mongoose = require("mongoose");

const competetionUser = new mongoose.Schema(
  {
    studentName: {
      type: String,
    },
    userId: {
      type: String,
      required: true,
    },
    userEmail: {
      type: String,
      required: true,
    },
    age: {
      type: Number,
    },
    city: {
      type: String,
    },
    albumId: {
      type: String,
    },
    // phone: {
    //   type: Number,
    // },
    standard: {
      type: String,
    },
    schoolName: {
      type: String,
    },
    // categories: {
    //   type: String,
    // },
    location: {
      type: String,
    },
    isPaid: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("CompetetionUser", competetionUser);
