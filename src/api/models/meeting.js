const mongoose = require("mongoose");

const meeting = mongoose.Schema(
  {
    meetingId: {
      type: String,
      // required: true,
    },
    liveCategory: {
      type: String,
      default: "liveSession",
    },
    meetingPlatform: {
      type: String,
    },
    meetingType: {
      type: String,
    },
    roomName: {
      type: String,
      // required: true,
    },
    hostName: {
      type: String,
      // required: true,
    },
    title: {
      type: String,
      // required: true,
    },
    status: {
      type: Boolean,
      default: true,
    },

    startAt: {
      type: Date,
      // required: true,
    },
    roleName: {
      type: String,
    },
    presetName: {
      type: String,
    },
    url: {
      type: String,
    },
    recordingUrl: {
      type: String,
    },
    hostProfile: {
      type: String,
    },
    bannerImage: {
      type: String,
    },
    description: {
      type: String,
    },
    endAt: {
      type: Date,
      // required: true,
    },
  },
  { timestamp: true }
);

module.exports = mongoose.model("Meeting", meeting);
