const mongoose = require("mongoose");

const meetingHosts = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    profileImage: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("meetingHosts", meetingHosts);
