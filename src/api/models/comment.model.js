const mongoose = require("mongoose");

const comment = new mongoose.Schema(
  {
    slug: {
      type: String,
      required: true,
    },
    comments: [
      {
        email: { type: String, required: true },
        name: { type: String, required: true },
        message: { type: String, required: true },
        createdAt: { type: Date, default: Date.now },
      },
    ],
  },
  { timestamps: true }
);

module.exports = mongoose.model("comment", comment);
