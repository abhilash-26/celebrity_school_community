const mongoose = require("mongoose");

const WebNotification = new mongoose.Schema(
  {
    message: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("WebNotification", WebNotification);
