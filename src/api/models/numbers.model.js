const mongoose = require("mongoose");

const numbers = mongoose.Schema(
  {
    name: {
      type: String,
    },

    number: {
      type: String,
    },
  },
  { timestamp: true }
);

module.exports = mongoose.model("Numbers", numbers);
