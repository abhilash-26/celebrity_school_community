const mongoose = require("mongoose");

const competitionUpload = new mongoose.Schema(
  {
    albumId: {
      type: Number,
      required: true,
    },
    description: {
      type: String,
    },
    userEmail: {
      type: String,
      required: true,
    },
    userName: {
      type: String,
    },
    fileUrl: {
      type: String,
      required: true,
    },
    reelStatus: {
      type: Boolean,
      default: false,
    },
    verified: {
      type: Boolean,
      default: false,
    },
    deep_link: {
      type: String,
    },
    likes: [],
    comments: [],
  },
  { timestamps: true }
);

module.exports = mongoose.model("CompetitionUpload", competitionUpload);
