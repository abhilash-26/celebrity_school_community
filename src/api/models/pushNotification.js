const mongoose = require("mongoose");

const pushNotification = new mongoose.Schema(
  {
    message: {
      type: String,
      required: true,
    },
    title: {
      type: String,
    },
    category: {
      type: String,
      required: true,
    },
    forUser: {
      type: String,
      default: "all",
    },
    userGroup: [],
    viewCount: [],
    author: {
      type: String,
    },
    courseId: {
      type: Number,
    },
    image: {
      type: String,
    },
    day: {
      type: String,
    },
    time: {
      type: String,
    },
    sent_time: {
      type: Date,
    },
    sent: {
      type: Boolean,
      default: false,
    },
    // expire_at: {
    //   type: Date,
    //   expires: 0,
    // },
  },
  { timestamps: true }
);

module.exports = mongoose.model("PushNotification", pushNotification);
