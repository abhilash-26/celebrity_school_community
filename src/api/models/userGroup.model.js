const mongoose = require("mongoose");

const userGroup = new mongoose.Schema(
  {
    groupId: {
      type: Number,
      required: true,
    },
    users: [],
  },
  { timestamps: true }
);

module.exports = mongoose.model("UserGroup", userGroup);
