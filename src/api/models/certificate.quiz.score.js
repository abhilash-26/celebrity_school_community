const mongoose = require("mongoose");

const quizScore = new mongoose.Schema(
  {
    userEmail: {
      type: String,
    },
    courseId: {
      type: Number,
    },
    score: {
      type: Number,
      required: true,
    },
    grade: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("QuizScore", quizScore);
