const mongoose = require("mongoose");

const schoolOlympiad = mongoose.Schema(
  {
    schoolEmail: { type: String },
    schoolName: { type: String },
    totalStudents: { type: String },
    mobile: { type: String },
    category: { type: String },
  },
  { timestamp: true }
);

module.exports = mongoose.model("School Olympiad", schoolOlympiad);
