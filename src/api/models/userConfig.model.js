const mongoose = require("mongoose");

const userConfig = mongoose.Schema(
  {
    userId: {
      type: Number,
      required: true,
    },
    interest: [
      {
        type: String,
      },
    ],
    questions: [],
    interestCaptured: {
      type: Boolean,
      default: false,
    },
    questionsRecorded: {
      type: Boolean,
      default: false,
    },
    visitCount: {
      type: Number,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("userConfig", userConfig);
