const mongoose = require("mongoose");

const userBlock = new mongoose.Schema(
  {
    userId: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("UserBlock", userBlock);
