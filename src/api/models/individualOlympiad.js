const mongoose = require("mongoose");

const individualOlympiad = mongoose.Schema(
  {
    parentEmail: { type: String },
    studentName: { type: String },
    standard: { type: String },
    mobile: { type: String },
    categories: { type: [String] },
  },
  { timestamp: true }
);

module.exports = mongoose.model("Individual Olympiad", individualOlympiad);
