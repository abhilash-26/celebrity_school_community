const mongoose = require("mongoose");

const featureFlag = new mongoose.Schema(
  {
    chatFeature: {
      type: Boolean,
      required: true,
      default: true,
    },
    notificationCenter: {
      type: Boolean,
      required: true,
      default: true,
    },
    communityModule: {
      type: Boolean,
      required: true,
      default: true,
    },
    competition: {
      type: Boolean,
      required: true,
      default: true,
    },
    profile: {
      type: Boolean,
      required: true,
      default: true,
    },
    restorePurchase: {
      type: Boolean,
      required: true,
      default: true,
    },
    couponCode: {
      type: Boolean,
      required: true,
      default: true,
    },
    webNotification: {
      type: Boolean,
      required: true,
      default: true,
    },
    appUpdate: {
      type: Boolean,
      required: true,
      default: true,
    },
    ios_version: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("FeatureFlag", featureFlag);
