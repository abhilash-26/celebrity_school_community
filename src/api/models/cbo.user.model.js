const mongoose = require("mongoose");

const userSchemaCbo = new mongoose.Schema(
  {
    email: {
      type: String,
      match: /^\S+@\S+\.\S+$/,
      trim: true,
      lowercase: true,
    },
    phone: {
      type: Number,
    },
    password: {
      type: String,
      required: true,
    },
    promocodes: [],
    name: {
      type: String,
      maxlength: 128,
      trim: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("UserCbo", userSchemaCbo);
