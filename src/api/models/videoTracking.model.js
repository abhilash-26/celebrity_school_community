const mongoose = require("mongoose");

const videoTrack = new mongoose.Schema(
  {
    userId: {
      type: Number,
      required: true,
    },
    courseId: {
      type: Number,
      required: true,
    },
    lessonId: {
      type: Number,
      required: true,
    },
    lessonProgress: {
      type: Number,
      required: true,
    },
    lessonLength: {
      type: Number,
      required: true,
    },
    completed: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("VideoTrack", videoTrack);
