const mongoose = require("mongoose");

const certificateQuiz = new mongoose.Schema(
  {
    courseId: {
      type: Number,
      required: true,
    },
    question: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    options: [],
    answer: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("CertificateQuiz", certificateQuiz);
