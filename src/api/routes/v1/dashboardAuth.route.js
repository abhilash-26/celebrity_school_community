const express = require("express");
const controller = require("../../controllers/dashboardAuth.controller");

const router = express.Router();

router.get("/", controller.getAuth);
router.post("/", controller.authenticate);

module.exports = router;
