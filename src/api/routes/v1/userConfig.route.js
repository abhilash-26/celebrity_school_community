const express = require("express");
const controller = require("../../controllers/userConfig.controller");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

router.post("/create-user-config", auth, controller.createUserConfig);

router.post("/update-user-interest", auth, controller.updateUserConfigIntrest);

router.post(
  "/update-user-questions",
  auth,
  controller.updateUserConfigQuestions
);

module.exports = router;
