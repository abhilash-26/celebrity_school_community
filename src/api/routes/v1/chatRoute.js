var Filter = require("bad-words");
filter = new Filter();
const logger = require("../../../config/logger");

const {
  saveMessage,
  fetchMessage,
  sendPrevMessage,
  fetchUser,
  deleteMessage,
} = require("../../controllers/chatController");

const chatModule = ({ io, socket }) => {
  let userdata = {};

  const id = socket.id;
  console.log("connected");

  socket.on("join", async ({ communityId, communityType }, useCallback) => {
    // logger.info(
    //   ` from join --  communityId -- ${communityId} communityType -- ${communityType}`
    // );

    userdata.communityId = communityId;
    userdata.communityType = communityType;
    const oldMessage = await fetchMessage(userdata.communityId);
    // logger.info(oldMessage);

    // socket.emit("message", {
    //   user: "admin",
    //   text: ` welcome to the chat `,
    // });
    socket.emit("message", {
      user: "admin",
      text: oldMessage ? oldMessage : "",
    });
    socket.join(userdata.communityId);

    // useCallback;
  });

  socket.on("deleteMessage", async ({ messageId, position }) => {
    const result = await deleteMessage(messageId);
    const oldMessage = await fetchMessage(userdata.communityId);
    io.to(userdata.communityId).emit("refreshMessage", {
      user: "admin",
      text: { messageId: messageId, position: position },
    });
  });

  //send message

  socket.on(
    "sendMessage",
    async ({ message, messageType, userName, userId, userRole }) => {
      logger.info("emitted");
      logger.info(
        ` from emit message -- message-${message} userId ${userId}  userName ${userName} tpe- ${messageType} role-${userRole}`
      );
      // emit message to the room assigned with communityId and clean message

      // let cleanMessage = filter.clean(message);

      // io.to(userdata.communityId).emit("message", {
      //   user: userName,
      //   text: cleanMessage,
      // });

      // saving message to db
      let roleBook = {
        143963: "14",
        143974: "3",
        144020: "7",
        144021: "12",
        144022: "8",
        144023: "13",
        144207: "9",
        144025: "6",
        144026: "10",
        144027: "16",
        144291: "1",
        144288: "4",
        144290: "15",
      };
      if (userRole == "admin") {
        if (roleBook[userId] != userdata.communityId) {
          userRole = "user";
        }
      }

      const result = await saveMessage({
        userId: userId,
        userName: userName,
        communityType: userdata.communityType,
        message,
        messageType,
        userRole,
        communityId: userdata.communityId,
      });
      io.to(userdata.communityId).emit("message", {
        user: userName,
        text: result
          ? [result]
          : [{ message: "unable to save message", flag: "FAIL" }],
      });
    }
  );
};

module.exports = { chatModule };
