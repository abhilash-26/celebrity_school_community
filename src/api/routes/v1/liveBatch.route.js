const express = require("express");
const controller = require("../../controllers/batch.controller");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

router.post("/create-user-batch", controller.createBatch);

router.get("/get-all-batch", controller.getAllBatch);

router.post("/add-user-to-batch", controller.adduserToBatch);

router.post("/remove-user-from-batch", controller.removeUserFromBatch);

module.exports = router;
