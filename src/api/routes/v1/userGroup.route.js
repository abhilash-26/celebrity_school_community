const express = require("express");
const router = express.Router();
const controller = require("../../controllers/userGroup.controller");

router.post("/add-user-group", controller.addToGroup);

module.exports = router;
