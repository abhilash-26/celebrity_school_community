const express = require("express");
const controller = require("../../controllers/cbo.user.controller");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

// router.post("/create", controller.create);

router.post("/register", controller.userSignUp);
router.post("/login", controller.userLogin);
router.get("/user-profile", auth, controller.getUserProfile);

module.exports = router;
