const express = require("express");
const router = express.Router();
const { fetchLatestMessage } = require("../../controllers/chatController");

router.post("/latest-message", fetchLatestMessage);

module.exports = router;
