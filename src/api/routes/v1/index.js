const express = require("express");
const userRoutes = require("./user.route");
const authRoutes = require("./auth.route");
const competetionRoute = require("./competionUser.Route");
const competetionSubmission = require("./competition.submission");
const featureFlagRoute = require("./featureFlag.route");
const chatRoute = require("./chat");
const webNotification = require("./webNotification.Route");
const pushNotification = require("./pushNotification.route");
const blog = require("./blog");
const meeting = require("./meeting.route");
const certificate = require("./certificateDownload.route");
const comment = require("./comment.route");
const videoTrack = require("./videoTracking.route");
const userGroup = require("./userGroup.route");
const userConfig = require("./userConfig.route");
const individualOlympiadConfig = require("./individual-olympiad.route");
const schoolOlympiadConfig = require("./school-olympiad.route");
const batch = require("./liveBatch.route");
const cbo = require("./cbo.user.route");
const quiz = require("./quiz.certificate.route");
const block = require("./user.block.route");
const oneOnOne = require("./oneOnOne.route");
const dashboardAuth = require("./dashboardAuth.route");
const numbers = require("./numbers.route");

const router = express.Router();

/**
 * GET v1/status
 */
router.get("/status", (req, res) => res.send("OK"));

/**
 * GET v1/docs
 */
router.use("/docs", express.static("docs"));

router.use("/users", userRoutes);
router.use("/auth", authRoutes);
router.use("/competition", competetionRoute);

router.use("/competition-submission", competetionSubmission);
router.use("/feature", featureFlagRoute);
router.use("/chat", chatRoute);
router.use("/notification", webNotification);
router.use("/push-notification", pushNotification);
router.use("/blog", blog);
router.use("/meeting", meeting);
router.use("/certificate", certificate);
router.use("/comment", comment);
router.use("/tracking", videoTrack);
router.use("/usergroup", userGroup);
router.use("/userconfig", userConfig);
router.use("/individual-olympiad", individualOlympiadConfig);
router.use("/school-olympiad", schoolOlympiadConfig);
router.use("/live-batch", batch);
router.use("/cbo", cbo);
router.use("/quiz", quiz);
router.use("/block", block);
router.use("/one-on-one", oneOnOne);
router.use("/dashboard-auth", dashboardAuth);
router.use("/numbers", numbers);

module.exports = router;
