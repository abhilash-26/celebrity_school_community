const express = require("express");
const controller = require("../../controllers/oneOnOne.controller");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

router.post("/save-user-detail", controller.createUserDetail);

router.post("/book-demo", auth, controller.createDemo);

router.post("/check-demo", controller.checkDemo);

router.get("/all-demo-booked", controller.demoBooked);

module.exports = router;
