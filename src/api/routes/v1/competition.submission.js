const express = require("express");
const validate = require("express-validation");
const controller = require("../../controllers/competition.controller");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

router.post("/upload-submission", controller.uploadSubmission);

router.get("/check-submission", controller.checkSubmission);

router.get("/view-submission", controller.getAllSubmission);

router.post("/verify-video", controller.verifyCompetitionSubmission);

router.get("/get-reels", auth, controller.getReals);

router.post("/reel-like", auth, controller.like);

module.exports = router;
