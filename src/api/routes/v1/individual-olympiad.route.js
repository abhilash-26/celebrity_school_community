const express = require("express");
const controller = require("../../controllers/olympiad.controller");

const router = express.Router();

router.post("/create-individual", controller.addIndividual);

module.exports = router;
