const express = require("express");
const controller = require("../../controllers/comment.controller");

const router = express.Router();

// router.post("/create", controller.create);

router.get("/get-comments/:slug", controller.getComments);
router.post("/add-comment/:slug", controller.addComment);

module.exports = router;
