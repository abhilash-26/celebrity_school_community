const express = require("express");
const validate = require("express-validation");
const controller = require("../../controllers/webNotification.controller");

const router = express.Router();

router.post("/create-web-notification", controller.create);

router.post("/update-web-notification", controller.update);

router.get("/get-web-notification", controller.getNotification);

router.get("/get-web-notification-new", controller.getWebNotification);

router.post("/get-single-notification", controller.getSingleNotification);

router.post("/get-multiple-notification", controller.getMultipleNotification);

router.post("/remove-web-notification", controller.deleteNotification);

module.exports = router;
