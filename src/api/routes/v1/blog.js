const express = require("express");
const controller = require("../../controllers/blog.user.controller");

const router = express.Router();

// router.post("/create", controller.create);

router.post("/create-user", controller.createUserDetails);
router.post("/create-email-newsletter", controller.createNewsletter);

module.exports = router;
