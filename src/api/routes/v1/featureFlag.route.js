const express = require("express");
const validate = require("express-validation");
const controller = require("../../controllers/featureFlag.controller");
// const { createCompetetionUser } = require("../../validations/user.validation");

const router = express.Router();

// router.post("/create", controller.create);

router.get("/get-app-config", controller.getFeatures);
router.post("/change-app-config", controller.changeStatus);

module.exports = router;
