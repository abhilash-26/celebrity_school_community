const express = require("express");
const controller = require("../../controllers/user.block.controller");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

router.post("/block-user", auth, controller.blockUser);

module.exports = router;
