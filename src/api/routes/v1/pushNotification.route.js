const express = require("express");
const validate = require("express-validation");
const controller = require("../../controllers/pushNotification.controller");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

router.post("/create-push-notification", controller.createNotification);
router.post("/update-push-notification", controller.updateNotification);
router.post("/remove-push-notification", controller.removePushNotification);
router.post("/send-push-notification", controller.sendNotification);
router.get("/get-all-push-notification", controller.getAllPushNotification);
router.get("/csv-download", controller.pushNotificationCsv);
router.post("/get-single-push-notification", controller.getSingleNotification);
router.get("/get-all-sent-push-notification", controller.getSentNotification);
router.post("/update-view-count", auth, controller.updateViewCount);

module.exports = router;
