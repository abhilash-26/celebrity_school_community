const express = require("express");
const controller = require("../../controllers/certificateDownload.controller");

const router = express.Router();

router.post(
  "/create-certificateDownload",
  controller.createDownloadCertificate
);

router.post("/certificateDownload-count", controller.getDownloadCountHitachi);
router.post(
  "/certificateDownload-count-serum-institute",
  controller.getDownloadCountSerumInstitute
);

router.get(
  "/certificateDownload-count-total",
  controller.getDownloadCountTotal
);

router.get(
  "/certificateDownload-count-per-course",
  controller.getDownloadCountPerCourse
);

module.exports = router;
