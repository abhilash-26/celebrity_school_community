const express = require("express");
const controller = require("../../controllers/numbers.controller");

const router = express.Router();

router.get("/", controller.get);
router.post("/", controller.create);

module.exports = router;
