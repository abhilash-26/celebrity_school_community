const express = require("express");
const controller = require("../../controllers/videoTracking.controller");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

router.post("/create-video-track", controller.createVideoTrack);

router.get("/average-watch-time", controller.getAverageWatchTime);

router.get(
  "/average-watch-time-hitachi-users",
  controller.getAverageWatchTimeHitachiUsers
);
router.get(
  "/average-watch-time-serum-institute-users",
  controller.getAverageWatchTimeSerumInstituteUsers
);

router.get("/average-watch-time-user", controller.getAverageWatchTimePerUser);

router.get(
  "/average-watch-time-user-app",
  auth,
  controller.getWatchTimeAppData
);

router.get(
  "/average-watch-time-user-detail",
  controller.getAverageWatchTimePerUserPerVideo
);

module.exports = router;
