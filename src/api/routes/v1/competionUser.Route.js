const express = require("express");
const validate = require("express-validation");
const controller = require("../../controllers/competionUser.controller");
const { createCompetetionUser } = require("../../validations/user.validation");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

// router.post(
//   "/payuCheckout",
//   validate(createCompetetionUser),
//   controller.create
// );

router.get("/get-all-users", controller.getAllUsers);
router.get("/users-json", controller.usersJson);
router.get("/users-registeration-json", controller.usersRegisterationJson);

router.post("/create-user-details", auth, controller.createCompetitionUser);

router.patch("/update-user-details", controller.updateCompetitionUser);

module.exports = router;
