const express = require("express");
const router = express.Router();
const contoller = require("../../controllers/webinar");

router.post("/create-meeting", contoller.createMeeting);

router.get("/get-meeting", contoller.getAllmeeting);

router.get("/get-meeting-premium", contoller.getAllmeetingPremium);
router.get("/get-all-meeting-dashboard", contoller.getAllMeetingdashboard);

router.put("/update-meeting", contoller.modifyMeeting);

router.put("/update-meeting-zoom", contoller.modifyMeetingZoom);

router.get(
  "/get-single-meeting/:roomName/:meetingId",
  contoller.getSingleMeeting
);

router.post("/remove-meeting", contoller.removeMeeting);

router.get("/get-all-hosts", contoller.getAllHosts);
router.post("/add-host", contoller.addHost);

module.exports = router;
