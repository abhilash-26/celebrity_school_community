const express = require("express");
const controller = require("../../controllers/quiz.certificate.controller");
const auth = require("../../middlewares/userAuth");

const router = express.Router();

// router.post("/create-quiz-question", controller.createQuestion);
router.get("/get-question", auth, controller.getQuestions);
router.post("/submit-answer", auth, controller.validateAnswer);

module.exports = router;
