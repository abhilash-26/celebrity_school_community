const FeatureFlag = require("../models/featureFlag.model");
const pushNotification = require("../models/pushNotification");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.create = async (req, res) => {
  try {
    const result = await FeatureFlag.create({
      chatFeature: true,
      notificationCenter: true,
      communityModule: true,
      competition: true,
      profile: true,
      restorePurchase: true,
      couponCode: true,
      webNotification: true,
      appUpdate: true,
    });
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getFeatures = async (req, res) => {
  try {
    const result = await FeatureFlag.findOne();
    const notificationCount = await pushNotification.countDocuments({
      sent: true,
    });
    const newResult = result.toObject();
    (newResult.notification_count = notificationCount),
      res.send({
        data: newResult,
        meta: {
          message: "Flag fetched are",
          flag: "SUCCESS",
          statuscode: 200,
        },
      });
    // sucessResponse(res, result, 200, "flag fetched are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.changeStatus = async (req, res) => {
  try {
    const { id, flagName } = req.body;

    if (!id || !flagName)
      return failResponse(res, null, 400, "flagName and id required");

    const tempData = await FeatureFlag.findById(id);

    const oldStatus = tempData[flagName];
    if (oldStatus == undefined) {
      return failResponse(res, null, 400, "no result found");
    }
    let newStatus = !oldStatus;
    const result = await FeatureFlag.findByIdAndUpdate(
      id,
      { [flagName]: newStatus },

      { new: true }
    );
    if (!result) {
      failResponse(res, null, 400, "unable to update plesae try again later");
    }
    sucessResponse(res, result, 200, "flag changed");
    return;
  } catch (error) {
    failResponse(res, null, 400, error.message);
    return;
  }
};
