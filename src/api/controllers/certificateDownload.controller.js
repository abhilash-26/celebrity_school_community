const CertificateDownload = require("../models/certificateDownload.model");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.createDownloadCertificate = async (req, res) => {
  try {
    const { userId, userEmail, courseId } = req.body;
    const existingDownload = await CertificateDownload.findOne({
      userId,
      courseId,
    });
    if (existingDownload) {
      return failResponse(res, null, 400, "Download already recorded");
    }
    const result = await CertificateDownload.create({
      userId,
      userEmail,
      courseId,
    });
    sucessResponse(res, result, 200, "User details recorded");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getDownloadCountHitachi = async (req, res) => {
  try {
    const { emailList, startDate, endDate } = req.body;
    let startAt = startDate,
      endAt = endDate;
    if (!startDate && !endDate) {
      startAt = new Date();
      startAt.setMonth(1);
      startAt.setDate(28);
      endAt = new Date();
    }
    const downloadCount = await CertificateDownload.countDocuments({
      userEmail: { $in: emailList },
      createdAt: { $gte: startAt, $lte: endAt },
    });
    sucessResponse(
      res,
      downloadCount,
      200,
      "Total certificate download count hitachi"
    );
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getDownloadCountSerumInstitute = async (req, res) => {
  try {
    const { emailList, startDate, endDate } = req.body;
    let startAt = startDate,
      endAt = endDate;
    if (!startDate && !endDate) {
      startAt = new Date();
      startAt.setMonth(1);
      startAt.setDate(28);
      endAt = new Date();
    }

    const downloads = await CertificateDownload.find({
      userEmail: { $in: emailList },
      createdAt: { $gte: startAt, $lte: endAt },
    });

    const downloadsDetail = downloads.map((item) => ({
      id: item._id,
      email: item.userEmail,
      courseId: item.courseId,
    }));

    const downloadCount = await CertificateDownload.countDocuments({
      userEmail: { $in: emailList },
      createdAt: { $gte: startAt, $lte: endAt },
    });
    sucessResponse(
      res,
      { downloadCount, downloadsDetail },
      200,
      "Total certificate download count hitachi"
    );
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getDownloadCountTotal = async (req, res) => {
  try {
    const { startDate, endDate } = req.body;
    let startAt = startDate,
      endAt = endDate;
    if (!startDate && !endDate) {
      startAt = new Date();
      startAt.setMonth(1);
      startAt.setDate(28);
      endAt = new Date();
    }
    const downloadCount = await CertificateDownload.countDocuments({
      // userEmail: { $in: emailList },
      createdAt: { $gte: startAt, $lte: endAt },
    });
    const dataTosend = {
      count: downloadCount * 800,
    };
    sucessResponse(res, dataTosend, 200, "Total certificate download count");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getDownloadCountPerCourse = async (req, res) => {
  try {
    const { startDate, endDate, albumId } = req.query;
    let startAt = startDate,
      endAt = endDate;
    if (!startDate && !endDate) {
      startAt = new Date();
      startAt.setMonth(1);
      startAt.setDate(28);
      startAt.setYear(2017);
      endAt = new Date();
    }
    const downloadCount = await CertificateDownload.countDocuments({
      // userEmail: { $in: emailList },
      createdAt: { $gte: startAt, $lte: endAt },
      courseId: albumId,
    });
    const dataTosend = {
      count: downloadCount,
    };
    sucessResponse(res, dataTosend, 200, "Total certificate download count");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
