const competitionUpload = require("../models/competitionUpload");
const CompetetionUser = require("../models/comepetionModel");

const { failResponse, sucessResponse } = require("../utils/responseHandler");

exports.uploadSubmission = async (req, res) => {
  try {
    const { fileUrl, albumId, userEmail, description } = req.body;
    let userName = null;
    const user = await CompetetionUser.findOne({
      userEmail,
    });
    if (user) {
      userName = user.studentName;
    }

    const submission = await competitionUpload.create({
      fileUrl,
      userName: userName,
      albumId,
      userEmail,
      description,
    });
    if (submission) {
      return sucessResponse(res, submission, 200, "submission sucessfull");
    }
    return failResponse(res, null, 400, "Something went wrong");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
exports.checkSubmission = async (req, res) => {
  try {
    const { userEmail, albumId } = req.body;

    const result = await competitionUpload.findOne({
      userEmail,
      albumId,
    });

    sucessResponse(res, result, 200, "found detail is");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getAllSubmission = async (req, res) => {
  try {
    const { albumId } = req.query;
    const allSubmission = await competitionUpload.find({
      albumId,
    });
    let responseData = {
      data: allSubmission,
      count: allSubmission.length,
    };

    sucessResponse(res, responseData, 200, "All submission");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.verifyCompetitionSubmission = async (req, res) => {
  try {
    const { id } = req.body;
    const tempUpload = await competitionUpload.findById(id);
    console.log(!tempUpload.verified);
    let status = !tempUpload.verified;

    const result = await competitionUpload.findByIdAndUpdate(id, {
      verified: !tempUpload.verified,
    });
    sucessResponse(res, null, 200, "Status changed");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

// reels

exports.getReals = async (req, res) => {
  try {
    let page = req.query.page || 1;
    const limit = 10;
    const skip = (page - 1) * limit;
    const userId = req.user.id;
    const reels = await competitionUpload
      .find({
        verified: true,
      })
      .limit(limit)
      .skip(skip);
    if (!reels.length > 0) {
      return failResponse(res, null, 400, "No reels found");
    }
    reels.sort((a, b) => {
      return b.likes.length - a.likes.length;
    });

    const data = reels.map((item) => {
      return {
        id: item._id,
        fileUrl: item.fileUrl,
        likeCount: item.likes.length,
        userName: item.userName ? item.userName : "user",
        description: item.description,
        deep_link: item.deep_link,
        like: item.likes.includes(userId) ? true : false,
      };
    });

    sucessResponse(res, data, 200, "All reels are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.like = async (req, res) => {
  try {
    const { id } = req.body;
    const userId = req.user.id;
    const tempLike = await competitionUpload.findOne({
      likes: userId,
    });
    if (tempLike) {
      const result2 = await competitionUpload.findByIdAndUpdate(
        id,
        {
          $pull: { likes: userId },
        },
        { new: true }
      );
      return sucessResponse(res, result2, 200, "like recorded");
    }
    const result = await competitionUpload.findByIdAndUpdate(
      id,
      {
        $addToSet: { likes: userId },
      },
      { new: true }
    );

    return sucessResponse(res, result, 200, "like recorded");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
