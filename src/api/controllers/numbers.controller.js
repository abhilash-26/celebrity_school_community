const NumbersModel = require("../models/numbers.model");
const AWS = require("aws-sdk");
const pushTemplates = require("../../utils/push-templates");
const {
  sucessResponse: successResponse,
  failResponse,
} = require("../utils/responseHandler");

exports.get = async (req, res) => {
  try {
    const result = await NumbersModel.findById("64bdf9af7c0ad423ec99fecd");

    successResponse(res, result, 200, "Data fetched successfully!");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.create = async (req, res) => {
  try {
    const { name, number } = req.body;
    if (!name && !number) throw new Error("Invalid body");

    await NumbersModel.findByIdAndUpdate("64bdf9af7c0ad423ec99fecd", {
      name,
      number,
    });

    const pinpoint = new AWS.Pinpoint({ region: "ap-south-1" });

    Object.keys(pushTemplates).forEach((key) => {
      const updatedContent = pushTemplates[key](name, number);
      pinpoint.updatePushTemplate(
        {
          PushNotificationTemplateRequest: {
            GCM: {
              RawContent: updatedContent,
            },
          },
          TemplateName: key,
          Version: "1",
          CreateNewVersion: false,
        },
        function (err, data) {
          if (err) console.log(err, err.stack);
          else console.log(data);
        }
      );
    });

    successResponse(res, {}, 200, "Created successfully!");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
