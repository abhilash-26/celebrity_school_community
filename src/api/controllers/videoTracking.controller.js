const VideoTrack = require("../models/videoTracking.model");
const { sucessResponse, failResponse } = require("../utils/responseHandler");
const axios = require("axios");

exports.createVideoTrack = async (req, res) => {
  try {
    const { courseId, userId, lessonId, lessonLength, lessonProgress } =
      req.body;
    if (!courseId || !userId || !lessonId || !lessonLength || !lessonProgress) {
      return failResponse(res, null, 400, "All fields are required");
    }
    const oldRecord = await VideoTrack.findOne({
      userId,
      courseId,
      lessonId,
    });
    if (oldRecord) {
      if (oldRecord.completed) {
        return sucessResponse(res, oldRecord, 200, "Course already completed");
      } else {
        let newProgress = oldRecord.lessonProgress + lessonProgress;
        let isCompleted = false;
        if (newProgress >= oldRecord.lessonLength) {
          newProgress = oldRecord.lessonLength;
          isCompleted = true;
        }
        const updatedRecord = await VideoTrack.findByIdAndUpdate(
          oldRecord._id,
          {
            lessonProgress: newProgress,
            completed: isCompleted,
          },
          { new: true }
        );
        return sucessResponse(
          res,
          updatedRecord,
          200,
          "updated course progress"
        );
      }
    }
    const newRecord = await VideoTrack.create({
      courseId,
      userId,
      lessonId,
      lessonLength,
      lessonProgress,
    });
    return sucessResponse(res, newRecord, 200, "New record created");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getAverageWatchTime = async (req, res) => {
  try {
    const result = await VideoTrack.aggregate([
      {
        $group: {
          _id: { courseId: "$courseId" },
          lessonProgress: { $sum: "$lessonProgress" },
          totalLessonLength: { $sum: "$lessonLength" },
          count: { $sum: 1 },
        },
      },
    ]);

    const allCourseLength = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/category/mobile_allLesson",
      headers: {
        "x-auth-token": "abc",
      },
    });

    const courseSoldData = {};

    for (
      let i = 0;
      i < allCourseLength.data.data.lessons[0].course.length;
      i++
    ) {
      const res1 = await VideoTrack.distinct("userId", {
        courseId: allCourseLength.data.data.lessons[0].course[i].id,
      });
      courseSoldData[allCourseLength.data.data.lessons[0].course[i].id] =
        res1.length;
    }

    // const soldCourseCount = await axios({
    //   method: "get",
    //   url: "https://www.origin.celebrityschool.in:1337/api/order/dashboard-number-of-course-sold-tracking",
    // });
    // let soldCourseObject = {};
    // soldCourseCount.data.data.map((item) => {
    //   soldCourseObject[item.ablum_id] = item.count;
    // });

    let lessonLengthSecond = {};
    allCourseLength.data.data.lessons[0].course.map((item) => {
      const len = item.total_length.split(" ");
      lessonLengthSecond[item.id] = len[0] * 60;
    });

    const viewTime = result.map((item) => {
      return {
        id: item._id.courseId,
        watchPercent:
          (item.lessonProgress /
            (lessonLengthSecond[item._id.courseId] *
              courseSoldData[item._id.courseId])) *
          100,
        lessonLength: lessonLengthSecond[item._id.courseId],
      };
    });
    return sucessResponse(res, viewTime, 200, "all record are");
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};

exports.getAverageWatchTimePerUser = async (req, res) => {
  try {
    const { userEmail } = req.query;
    const user = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/user/fetch_user_profile",
      headers: {
        "x-auth-token": "abc",
        user_email: userEmail,
      },
    });

    if (user.data.meta.flag == "FAIL")
      return failResponse(
        res,
        null,
        400,
        "No user found with email id entered"
      );
    const userId = user.data.data.profile.id;
    // let purchasedCourseObjectToSend = {};
    const allCoursePurchased = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/order/dashboard-tracking-all-purchased-course",
      params: { email: userEmail },
    });

    const allCourseLength = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/category/mobile_allLesson",
      headers: {
        "x-auth-token": "abc",
      },
    });

    let lessonLengthSecond = {};
    const CourseLengthTotal = allCourseLength.data.data.lessons[0].course.map(
      (item) => {
        const len = item.total_length.split(" ");
        lessonLengthSecond[item.id] = len[0] * 60;
        return {
          courseId: item.id,
          length: len[0] * 100,
        };
      }
    );
    // if (allCoursePurchased.data.data.courses == 0) {
    // }
    const result = await VideoTrack.aggregate([
      {
        $match: { userId: userId },
      },
      {
        $group: {
          _id: { courseId: "$courseId" },
          lessonProgress: { $sum: "$lessonProgress" },
          totalLessonLength: { $sum: "$lessonLength" },
        },
      },
    ]);
    const viewTime = result.map((item) => {
      return {
        id: item._id.courseId,
        // watchPercent: (item.lessonProgress / item.totalLessonLength) * 100,
        watchPercent:
          (item.lessonProgress / lessonLengthSecond[item._id.courseId]) * 100,
      };
    });

    for (let i = 0; i < result.length; i++) {
      for (
        let j = 0;
        j < allCourseLength.data.data.lessons[0].course.length;
        j++
      ) {
        if (
          result[i]._id.courseId ==
          allCourseLength.data.data.lessons[0].course[j].id
        ) {
          allCourseLength.data.data.lessons[0].course[j]["watchPercentage"] =
            (result[i].lessonProgress /
              lessonLengthSecond[result[i]._id.courseId]) *
            100;
        }
      }
    }

    // let dataToSend = {
    //   coursePurchased:
    //     allCoursePurchased.data.data.courses == 0
    //       ? allCourseLength.data.data.lessons[0].course
    //       : allCoursePurchased.data.data,
    //   courseCompletionRate: viewTime.length > 0 ? viewTime : 0,
    // };
    // console.log(allCoursePurchased.data.data.courses);
    // let onlyBoughtCourse;
    // if(allCoursePurchased.data.data.courses != 0){
    //   onlyBoughtCourse =   allCourseLength.data.data.lessons[0].course.filter((item) => {

    //   });
    // }

    let dataToSend;
    console.log(allCoursePurchased.data.data.courses);

    if (allCoursePurchased.data.data.courses == 0) {
      dataToSend = allCourseLength.data.data.lessons[0].course.map((item) => {
        return {
          id: item.id,
          meta_title: item.meta_title,
          viewTime: item.watchPercentage ? item.watchPercentage : 0,
        };
      });
    } else {
      for (let i = 0; i < allCoursePurchased.data.data.courses.length; i++) {
        for (let j = 0; j < viewTime.length; j++) {
          if (allCoursePurchased.data.data.courses[i].id == viewTime[j].id) {
            allCoursePurchased.data.data.courses[i]["viewTime"] =
              viewTime[j].watchPercent;
          }
        }
      }
      dataToSend = allCoursePurchased.data.data.courses.map((item) => {
        return {
          id: item.id,
          // meta_title: item.meta_title,
          viewTime: item.viewTime ? item.viewTime : 0,
        };
      });
    }

    // let dataToSend =
    //   allCoursePurchased.data.data.courses == 0
    //     ? allCourseLength.data.data.lessons[0].course
    //     : allCourseLength.data.data.lessons[0].course.filter((item) => {});
    sucessResponse(
      res,
      // allCourseLength.data.data.lessons[0].course,
      dataToSend,
      200,
      "all record are"
    );
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};

exports.getAverageWatchTimePerUserPerVideo = async (req, res) => {
  try {
    const { userEmail, courseId } = req.query;
    const user = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/user/fetch_user_profile",
      headers: {
        "x-auth-token": "abc",
        user_email: userEmail,
      },
    });

    if (user.data.meta.flag == "FAIL")
      return failResponse(
        res,
        null,
        400,
        "No user found with email id entered"
      );
    const userId = user.data.data.profile.id;
    const allLessons = await axios({
      method: "get",
      url: `https://www.origin.celebrityschool.in:1337/api/v1/video/album_video_lessons/${courseId}`,
      headers: {
        "x-auth-token": "abc",
      },
    });
    const result = await VideoTrack.find({
      userId,
      courseId,
    });

    // allLessons.data.data.video_lessons[j]["viewTime"] = 0;

    for (let i = 0; i < result.length; i++) {
      for (let j = 0; j < allLessons.data.data.video_lessons.length; j++) {
        if (result[i].lessonId == allLessons.data.data.video_lessons[j].id) {
          allLessons.data.data.video_lessons[j]["viewTime"] =
            (result[i].lessonProgress / result[i].lessonLength) * 100;
        }
      }
    }

    const dataToSend = allLessons.data.data.video_lessons.map((item) => {
      return {
        id: item.id,
        album_id: item.album_id,
        video_title: item.video_title,

        video_place: item.video_place,

        viewTime: item.viewTime ? item.viewTime : 0,
      };
    });

    sucessResponse(
      res,
      // allLessons.data.data.video_lessons,
      dataToSend,
      200,
      "all record are"
    );
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};

exports.getAverageWatchTimeHitachiUsers = async (req, res) => {
  try {
    const userList = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/order/all-hitachi-user-list",
    });
    let userArray = userList.data.data.map((item) => item.id);

    const result = await VideoTrack.aggregate([
      {
        $match: {
          userId: { $in: userArray },
        },
      },
      {
        $group: {
          _id: { courseId: "$courseId" },
          lessonProgress: { $sum: "$lessonProgress" },
          totalLessonLength: { $sum: "$lessonLength" },
          count: { $sum: 1 },
        },
      },
    ]);

    const allCourseLength = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/category/mobile_allLesson",
      headers: {
        "x-auth-token": "abc",
      },
    });

    const courseSoldData = {};

    // for (
    //   let i = 0;
    //   i < allCourseLength.data.data.lessons[0].course.length;
    //   i++
    // ) {
    //   const res1 = await VideoTrack.distinct("userId", {
    //     courseId: allCourseLength.data.data.lessons[0].course[i].id,
    //   });
    //   courseSoldData[allCourseLength.data.data.lessons[0].course[i].id] =
    //     res1.length;
    // }

    // const soldCourseCount = await axios({
    //   method: "get",
    //   url: "https://www.origin.celebrityschool.in:1337/api/order/dashboard-number-of-course-sold-tracking",
    // });
    // let soldCourseObject = {};
    // soldCourseCount.data.data.map((item) => {
    //   soldCourseObject[item.ablum_id] = item.count;
    // });

    let lessonLengthSecond = {};
    allCourseLength.data.data.lessons[0].course.map((item) => {
      const len = item.total_length.split(" ");
      lessonLengthSecond[item.id] = len[0] * 60;
    });

    const viewTime = result.map((item) => {
      return {
        id: item._id.courseId,
        watchPercent:
          (item.lessonProgress /
            (lessonLengthSecond[item._id.courseId] * userArray.length)) *
          100,
        lessonLength: lessonLengthSecond[item._id.courseId],
      };
    });
    return sucessResponse(res, viewTime, 200, "all record are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getAverageWatchTimeSerumInstituteUsers = async (req, res) => {
  try {
    const userList = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/order/all-serum-institute-user-list",
    });
    let userArray = userList.data.data.map((item) => item.id);

    const result = await VideoTrack.aggregate([
      {
        $match: {
          userId: { $in: userArray },
        },
      },
      {
        $group: {
          _id: { courseId: "$courseId" },
          lessonProgress: { $sum: "$lessonProgress" },
          totalLessonLength: { $sum: "$lessonLength" },
          count: { $sum: 1 },
        },
      },
    ]);

    const allCourseLength = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/category/mobile_allLesson",
      headers: {
        "x-auth-token": "abc",
      },
    });

    const courseSoldData = {};

    // for (
    //   let i = 0;
    //   i < allCourseLength.data.data.lessons[0].course.length;
    //   i++
    // ) {
    //   const res1 = await VideoTrack.distinct("userId", {
    //     courseId: allCourseLength.data.data.lessons[0].course[i].id,
    //   });
    //   courseSoldData[allCourseLength.data.data.lessons[0].course[i].id] =
    //     res1.length;
    // }

    // const soldCourseCount = await axios({
    //   method: "get",
    //   url: "https://www.origin.celebrityschool.in:1337/api/order/dashboard-number-of-course-sold-tracking",
    // });
    // let soldCourseObject = {};
    // soldCourseCount.data.data.map((item) => {
    //   soldCourseObject[item.ablum_id] = item.count;
    // });

    let lessonLengthSecond = {};
    allCourseLength.data.data.lessons[0].course.map((item) => {
      const len = item.total_length.split(" ");
      lessonLengthSecond[item.id] = len[0] * 60;
    });

    const viewTime = result.map((item) => {
      return {
        id: item._id.courseId,
        watchPercent:
          (item.lessonProgress /
            (lessonLengthSecond[item._id.courseId] * userArray.length)) *
          100,
        lessonLength: lessonLengthSecond[item._id.courseId],
      };
    });
    return sucessResponse(res, viewTime, 200, "all record are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getWatchTimeAppData = async (req, res) => {
  try {
    // console.log(req.user);
    const userEmail = req.user.email;
    const user = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/user/fetch_user_profile",
      headers: {
        "x-auth-token": "abc",
        user_email: userEmail,
      },
    });

    if (user.data.meta.flag == "FAIL")
      return failResponse(
        res,
        null,
        400,
        "No user found with email id entered"
      );
    const userId = user.data.data.profile.id;
    // let purchasedCourseObjectToSend = {};
    const allCoursePurchased = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/order/dashboard-tracking-all-purchased-course",
      params: { email: userEmail },
    });

    const allCourseLength = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/category/mobile_allLesson",
      headers: {
        "x-auth-token": "abc",
      },
    });

    let lessonLengthSecond = {};
    const CourseLengthTotal = allCourseLength.data.data.lessons[0].course.map(
      (item) => {
        const len = item.total_length.split(" ");
        lessonLengthSecond[item.id] = len[0] * 60;
        return {
          courseId: item.id,
          length: len[0] * 100,
        };
      }
    );
    // if (allCoursePurchased.data.data.courses == 0) {
    // }
    const result = await VideoTrack.aggregate([
      {
        $match: { userId: userId },
      },
      {
        $group: {
          _id: { courseId: "$courseId" },
          lessonProgress: { $sum: "$lessonProgress" },
          totalLessonLength: { $sum: "$lessonLength" },
        },
      },
    ]);
    const viewTime = result.map((item) => {
      return {
        id: item._id.courseId,
        // watchPercent: (item.lessonProgress / item.totalLessonLength) * 100,
        watchPercent:
          (item.lessonProgress / lessonLengthSecond[item._id.courseId]) * 100,
      };
    });

    for (let i = 0; i < result.length; i++) {
      for (
        let j = 0;
        j < allCourseLength.data.data.lessons[0].course.length;
        j++
      ) {
        if (
          result[i]._id.courseId ==
          allCourseLength.data.data.lessons[0].course[j].id
        ) {
          allCourseLength.data.data.lessons[0].course[j]["watchPercentage"] =
            (result[i].lessonProgress /
              lessonLengthSecond[result[i]._id.courseId]) *
            100;
        }
      }
    }

    // let dataToSend = {
    //   coursePurchased:
    //     allCoursePurchased.data.data.courses == 0
    //       ? allCourseLength.data.data.lessons[0].course
    //       : allCoursePurchased.data.data,
    //   courseCompletionRate: viewTime.length > 0 ? viewTime : 0,
    // };
    // console.log(allCoursePurchased.data.data.courses);
    // let onlyBoughtCourse;
    // if(allCoursePurchased.data.data.courses != 0){
    //   onlyBoughtCourse =   allCourseLength.data.data.lessons[0].course.filter((item) => {

    //   });
    // }

    let dataToSend;
    console.log(allCoursePurchased.data.data.courses);

    if (allCoursePurchased.data.data.courses == 0) {
      dataToSend = allCourseLength.data.data.lessons[0].course.map((item) => {
        return {
          id: item.id,
          meta_title: item.meta_title,
          viewTime: item.watchPercentage ? item.watchPercentage : 0,
        };
      });
    } else {
      for (let i = 0; i < allCoursePurchased.data.data.courses.length; i++) {
        for (let j = 0; j < viewTime.length; j++) {
          if (allCoursePurchased.data.data.courses[i].id == viewTime[j].id) {
            allCoursePurchased.data.data.courses[i]["viewTime"] =
              viewTime[j].watchPercent;
          }
        }
      }
      dataToSend = allCoursePurchased.data.data.courses.map((item) => {
        return {
          id: item.id,
          // meta_title: item.meta_title,
          viewTime: item.viewTime ? item.viewTime : 0,
        };
      });
    }

    // let dataToSend =
    //   allCoursePurchased.data.data.courses == 0
    //     ? allCourseLength.data.data.lessons[0].course
    //     : allCourseLength.data.data.lessons[0].course.filter((item) => {});
    sucessResponse(
      res,
      // allCourseLength.data.data.lessons[0].course,
      dataToSend,
      200,
      "all record are"
    );
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};
