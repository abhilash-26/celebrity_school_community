const Comment = require("../models/comment.model");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.getComments = async (req, res) => {
  try {
    const { slug } = req.params;

    const result = await Comment.findOne({ slug });

    if (result) {
      const comments = result.comments;

      sucessResponse(
        res,
        {
          comments,
          total: comments.length,
        },
        200,
        "Get all comments"
      );
    }

    return sucessResponse(
      res,
      {
        comments: [],
        total: 0,
      },
      200,
      "Get all comments"
    );
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.addComment = async (req, res) => {
  try {
    const { slug } = req.params;
    const { email, name, message } = req.body;

    const post = await Comment.findOne({ slug });

    if (post) {
      const result = await Comment.findOneAndUpdate(
        { slug },
        { comments: [{ email, name, message }, ...post.comments] }
      );

      return sucessResponse(res, result, 200, "Comment added");
    }

    const result = await Comment.create({
      slug,
      comments: [{ email, name, message }],
    });

    return sucessResponse(res, result, 200, "Comment added");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
