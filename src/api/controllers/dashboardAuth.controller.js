const jwt = require("jsonwebtoken");

exports.getAuth = function (req, res) {
  const token = req.headers["x-auth-token"];

  if (!token) return res.status(400).json({ msg: "No token found!" });

  const decoded = jwt.decode(token.toString());

  if (
    decoded &&
    decoded.email &&
    decoded.email === "support@celebrityschool.in"
  )
    return res.status(200).json({ email: decoded.email, token });

  return res.status(400).json({ msg: "Invalid token!" });
};

exports.authenticate = function (req, res) {
  const {
    body: { authEmail, authPassword },
  } = req;

  if (
    authEmail === "support@celebrityschool.in" &&
    authPassword === "Celebrity@123"
  ) {
    const token = jwt.sign({ email: authEmail }, "shhh");
    res.status(200).json({ token, email: authEmail });
    return;
  }

  res.status(400).json({ error: "Invalid email or password" });
};
