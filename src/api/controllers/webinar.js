const Meeting = require("../models/meeting");
const Hosts = require("../models/meetingHosts.model");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.webinarModule = ({ io, socket }) => {
  const id = socket.id;
  console.log(id);

  socket.on("join-webinar", async ({ userId }, useCallback) => {
    console.log("working");
    socket.emit("message-webinar", {
      user: "webinar admin",
      text: ` welcome to the chat `,
    });
    console.log("her");
    console.log(userId);
    socket.join(userId);

    // useCallback;
  });

  //send message

  socket.on("send-webinar", async ({ userId }) => {
    // emit message to the room assigned with communityId and clean message

    io.to(userId).emit("message-webinar", {
      user: "abhilash",
      text: "wel",
    });

    // saving message to db

    // io.to(userdata.communityId).emit("message", {
    //   user: userName,
    //   text: result
    //     ? [result]
    //     : [{ message: "unable to save message", flag: "FAIL" }],
    // });
  });
};

exports.createMeeting = async (req, res) => {
  try {
    const {
      meetingId,
      meetingType,
      roomName,
      title,
      startAt,
      endAt,
      hostName,
      roleName,
      presetName,
      url,
      hostProfile,
      description,
      bannerImage,
      meetingPlatform,
      liveCategory,
      status,
    } = req.body;
    const result = await Meeting.create({
      meetingId,
      meetingType,
      roomName,
      meetingPlatform,
      title,
      startAt,
      endAt,
      hostName,
      roleName,
      presetName,
      url,
      bannerImage,
      description,
      hostProfile,
      liveCategory,
      status,
    });
    sucessResponse(res, result, 200, "Meeting created");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getAllmeeting = async (req, res) => {
  try {
    const result = await Meeting.find().sort({ startAt: -1 });

    const dataToSend = result.filter(
      (item) =>
        (item.recordingUrl && item.status == false) || item.status == true
    );
    if (result) sucessResponse(res, dataToSend, 200, "All meetings are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getAllmeetingPremium = async (req, res) => {
  try {
    const result = await Meeting.find({
      liveCategory: "premium",
    }).sort({ startAt: -1 });

    if (result) sucessResponse(res, result, 200, "All meetings are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getSingleMeeting = async (req, res) => {
  try {
    const roomName = req.params.roomName || "";
    const meetingId = req.params.meetingId || "";
    const result = await Meeting.findOne({
      meetingId,
      roomName,
    });
    if (result) return sucessResponse(res, result, 200, "meeting found is");
    failResponse(res, null, 400, "No meeting found");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.modifyMeeting = async (req, res) => {
  try {
    const { meetingId, outputFileName } = req.body;
    const s3url = `https://celebrityschoolmeetingrecording.s3.ap-south-1.amazonaws.com/${outputFileName}`;
    const result = await Meeting.findOneAndUpdate(
      {
        meetingId,
      },
      { status: false, recordingUrl: s3url },
      { new: true }
    );
    if (result) return sucessResponse(res, result, 200, "updated document");
    failResponse(res, null, 400, "Unable to update please try again later");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
exports.modifyMeetingZoom = async (req, res) => {
  try {
    const { id, outputFileName } = req.body;
    const result = await Meeting.findByIdAndUpdate(
      id,
      { status: false, recordingUrl: outputFileName },
      { new: true }
    );
    if (result) return sucessResponse(res, result, 200, "updated document");
    failResponse(res, null, 400, "Unable to update please try again later");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getAllMeetingdashboard = async (req, res) => {
  try {
    let limit = req.query.limit;
    let searchvalue = req.query.searchvalue;
    let sortBy = req.query.sortBy;
    let sort = req.query.sort;
    let reg = "";
    if (searchvalue) {
      reg = searchvalue;
    }

    const allFilterSearch = {
      $or: [
        {
          hostName: {
            $regex: reg,
            $options: "i",
          },
        },
        {
          title: {
            $regex: reg,
            $options: "i",
          },
        },
      ],
    };
    let limitTo = parseInt(limit);
    const page = req.query.page;
    let skip = parseInt(limit) * (parseInt(page) - 1);
    const result = await Meeting.find(
      allFilterSearch,
      "title hostName roomName status recordingUrl"
    )
      .sort({ startAt: -1 })
      .limit(limitTo)
      .skip(skip);

    const count = await Meeting.countDocuments(allFilterSearch);
    let responseData = {
      data: result,
      count: count,
    };
    sucessResponse(res, responseData, 200, "all meetings are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.removeMeeting = async (req, res) => {
  try {
    const { id } = req.body;
    const result = await Meeting.findByIdAndRemove(id);
    if (!result)
      return failResponse(
        res,
        null,
        400,
        "unable to find meeting please try again later"
      );
    return sucessResponse(res, result, 200, "meeting removed");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getAllHosts = async (req, res) => {
  try {
    const result = await Hosts.find().sort({ startAt: -1 });

    // const dataToSend = result.filter();
    if (result) sucessResponse(res, result, 200, "All hosts are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.addHost = async (req, res) => {
  try {
    const { name, profileImage } = req.body;
    const result = await Hosts.create({
      name,
      profileImage,
    });
    sucessResponse(res, result, 200, "Host created");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
