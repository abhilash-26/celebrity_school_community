const BlogUser = require("../models/blog.user.model");
const BlogNewsLetter = require("../models/blog.newsLetter");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.createUserDetails = async (req, res) => {
  try {
    const { firstName, lastName, mobile, zip, message } = req.body;
    if (!firstName || !lastName || !mobile || !zip || !message)
      return failResponse(res, null, 400, "All fields are required");
    const result = await BlogUser.create({
      firstName,
      lastName,
      mobile,
      zip,
      message,
    });
    if (!result)
      return failResponse(
        res,
        null,
        400,
        "something went wrong please try again later"
      );
    return sucessResponse(res, result, 200, "User details recorded");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.createNewsletter = async (req, res) => {
  try {
    const { email } = req.body;
    if (!email) return failResponse(res, null, 400, "Email is required");
    const tempUser = await BlogNewsLetter.findOne({ email });
    if (tempUser) {
      return sucessResponse(res, tempUser, 200, "Newsletter email found");
    }
    const result = await BlogNewsLetter.create({
      email,
    });
    if (!result)
      return failResponse(
        res,
        null,
        400,
        "Something went wrong please try again later"
      );
    return sucessResponse(res, result, 200, "Newsletter email collected");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
