const userConfig = require("../models/userConfig.model");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

const categoryList = [
  {
    name: "Direction",
    image: "https://mymedia01.s3.ap-south-1.amazonaws.com/png/director_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_director_.png",
  },
  {
    name: "Acting",
    image: "https://mymedia01.s3.ap-south-1.amazonaws.com/png/acting_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/whte_acting_.png",
  },
  {
    name: "Business",
    image: "https://mymedia01.s3.ap-south-1.amazonaws.com/png/business_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_business_.png",
  },
  {
    name: "Singing",
    image:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/music+artist_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_singing_.png",
  },
  {
    name: "Communication",
    image:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/communication_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_communication_.png",
  },
  {
    name: "Photography",
    image: "https://mymedia01.s3.ap-south-1.amazonaws.com/png/camera_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_camera_.png",
  },
  {
    name: "Cooking",
    image: "https://mymedia01.s3.ap-south-1.amazonaws.com/png/cooking_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_cooking_.png",
  },
  {
    name: "Dance",
    image: "https://mymedia01.s3.ap-south-1.amazonaws.com/png/dancing_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_dancing_.png",
  },
  {
    name: "MakeUp and Hair",
    image:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/Makeupand_hair_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_makeupandhair_.png",
  },
  {
    name: "Modern Cooking",
    image:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/moderncooking_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_moderncooking_.png",
  },
  {
    name: "Confidence Building",
    image:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/confidence+building_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_confidence+building_.png",
  },
  {
    name: "Writing",
    image: "https://mymedia01.s3.ap-south-1.amazonaws.com/png/writing_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_writing_.png",
  },
  {
    name: "Singing Foundation",
    image:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/music+artist_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_singing_.png",
  },
  {
    name: "Digital 101",
    image:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/Digital+Age101_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_Global+Digital+age101_.png",
  },
  {
    name: "Fitness",
    image: "https://mymedia01.s3.ap-south-1.amazonaws.com/png/fitness_.png",
    image2:
      "https://mymedia01.s3.ap-south-1.amazonaws.com/png/white_fitness_.png",
  },
];

exports.createUserConfig = async (req, res) => {
  try {
    const userId = req.user.id;
    const existingUser = await userConfig.findOne({
      userId,
    });
    if (existingUser) {
      const updatedUser = await userConfig.findOneAndUpdate(
        {
          userId,
        },
        {
          visitCount: existingUser.visitCount + 1,
        },
        { new: true }
      );
      const dataToSend = {
        user: updatedUser,
        category: categoryList,
      };
      return sucessResponse(res, dataToSend, 200, "Visit count updated");
    }
    const result = await userConfig.create({
      userId,
      visitCount: 1,
    });
    const dataToSend = {
      user: result,
      category: categoryList,
    };
    sucessResponse(res, dataToSend, 200, "User config created");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

// exports.getUserConfig = async (req, res) => {
//   try {
//     const result = await userConfig.findOne({
//       userId,
//     });
//     console.log(result);
//   } catch (error) {
//     failResponse(res, null, 400, error.message);
//   }
// };

exports.updateUserConfigIntrest = async (req, res) => {
  try {
    const userId = req.user.id;
    const { interest } = req.body;
    if (!interest) {
      return failResponse(res, null, 400, "Interest is required");
    }
    const result = await userConfig.findOneAndUpdate(
      {
        userId,
      },
      { interest: interest, interestCaptured: true },
      { new: true }
    );
    sucessResponse(res, result, 200, "User interest updated");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.updateUserConfigQuestions = async (req, res) => {
  try {
    const userId = req.user.id;
    const { questions } = req.body;
    if (!questions) {
      return failResponse(res, null, 400, "question array is required");
    }
    const result = await userConfig.findOneAndUpdate(
      {
        userId,
      },
      { questions: questions, questionsRecorded: true },
      { new: true }
    );
    sucessResponse(res, result, 200, "User answer recorded");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
