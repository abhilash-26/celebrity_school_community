const WebNotification = require("../models/webNotificationModel");
const FeatureFlag = require("../models/featureFlag.model");

const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.create = async (req, res) => {
  try {
    const result = await WebNotification.create({
      message: req.body.message,
    });
    sucessResponse(res, result, 200, "created message");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.update = async (req, res) => {
  const { message, status } = req.body;

  try {
    const result = await WebNotification.findByIdAndUpdate(
      req.body.id,
      {
        message,
      },
      { new: true }
    );

    const flagResult = await FeatureFlag.findByIdAndUpdate(
      "62177dac3e5c7615105efa1e",
      { webNotification: status },

      { new: true }
    );

    sucessResponse(res, result, 200, "updated message");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getNotification = async (req, res) => {
  try {
    let limit = req.query.limit;
    let limitTo = parseInt(limit);
    const page = req.query.page;
    let skip = parseInt(limit) * (parseInt(page) - 1);
    const result = await WebNotification.find().limit(limitTo).skip(skip);
    sucessResponse(res, result, 200, "web notifications are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getWebNotification = async (req, res) => {
  try {
    let limit = req.query.limit;
    let limitTo = parseInt(limit);
    const page = req.query.page;
    let skip = parseInt(limit) * (parseInt(page) - 1);
    const result = await WebNotification.find().limit(limitTo).skip(skip);
    const webFlag = await FeatureFlag.findOne();
    const dataToSend = {
      data: result,
      flag: webFlag.webNotification,
    };
    sucessResponse(res, dataToSend, 200, "web notifications are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.deleteNotification = async (req, res) => {
  try {
    const { id } = req.body;
    async function DeleteAllNotification(id) {
      let result1 = [];
      for (let i = 0; i < id.length; i++) {
        console.log(id[i]);
        const result = await WebNotification.findByIdAndRemove(id[i]);
        result1[i] = result;
      }
      return result1;
    }
    const dataTosend = await DeleteAllNotification(id);
    sucessResponse(
      res,
      dataTosend,
      200,
      "web notifications deleted sucessfully"
    );
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getSingleNotification = async (req, res) => {
  try {
    const { id } = req.body;
    if (!id) return failResponse(res, null, 400, "Id is required");
    const result = await WebNotification.findById(id);
    if (!result)
      return failResponse(
        res,
        null,
        400,
        "no notification found with given id"
      );
    return sucessResponse(res, result, 200, "web notifications are");
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};

exports.getMultipleNotification = async (req, res) => {
  try {
    const { id } = req.body;
    console.log(id);
    async function findAllNotification(id) {
      let result1 = [];
      for (let i = 0; i < id.length; i++) {
        console.log(id[i]);
        const result = await WebNotification.findById(id[i]);
        result1[i] = result;
      }
      return result1;
    }
    const dataTosend = await findAllNotification(id);

    return sucessResponse(res, dataTosend, 200, "web notifications are");
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};
