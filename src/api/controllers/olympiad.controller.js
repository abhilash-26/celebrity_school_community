const IndividualOlympiad = require("../models/individualOlympiad");
const SchoolOlympiad = require("../models/schoolOlympiad");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.addIndividual = async (req, res) => {
  try {
    const { studentName, parentEmail, mobile, standard, categories } = req.body;
    const result = await IndividualOlympiad.create({
      studentName,
      parentEmail,
      mobile,
      standard,
      categories,
    });
    sucessResponse(res, result, 200, "Student record created");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.addSchool = async (req, res) => {
  try {
    const { schoolName, schoolEmail, mobile, totalStudents, category } =
      req.body;
    const result = await SchoolOlympiad.create({
      schoolName,
      schoolEmail,
      mobile,
      totalStudents,
      category,
    });
    sucessResponse(res, result, 200, "School record created");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
