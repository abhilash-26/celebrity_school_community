const CompetetionUser = require("../models/comepetionModel");
const crypto = require("crypto");

const axios = require("axios");

const { sucessResponse, failResponse } = require("../utils/responseHandler");

// function generateOrderID(length) {
//   var randomChars =
//     "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
//   var result = "";
//   for (var i = 0; i < length; i++) {
//     result += randomChars.charAt(
//       Math.floor(Math.random() * randomChars.length)
//     );
//   }
//   return result;
// }

// exports.create = async (req, res) => {
//   try {
//     const { name, email, phone, group, schoolName, categories } = req.body;

//     const key = "XdtbvU";
//     const salt = "IlLL0R2y";

//     const txnid = `order${generateOrderID(15)}`;
//     let price = 200;

//     if (email === "test@example4400.com") price = 1;

//     const text =
//       key +
//       "|" +
//       txnid +
//       "|" +
//       price +
//       "|" +
//       "Celebrityschool Competition" +
//       "|" +
//       name +
//       "|" +
//       email +
//       "|" +
//       categories +
//       "|" +
//       group +
//       "|" +
//       schoolName +
//       "||" +
//       "Celebrityschool Competition" +
//       "||||||" +
//       salt;

//     const cryp = crypto.createHash("sha512");
//     cryp.update(text);
//     const hash = cryp.digest("hex");

//     const response = {
//       txnid,
//       hash,
//       price,
//       key,
//       email,
//       firstname: name,
//       phone,
//       categories,
//       group,
//       schoolName,
//       productinfo: "Celebrityschool Competition",
//     };

//     sucessResponse(res, response, 200, "user profile created ");
//   } catch (error) {
//     failResponse(res, null, 401, error.message);
//   }
// };

// exports.response = async (req, res) => {
//   try {
//     if (req.body.status === "success") {
//       const user = await CompetetionUser.create({
//         name: req.body.firstname,
//         email: req.body.email,
//         phone: req.body.phone,
//         categories: req.body.udf1,
//         group: req.body.udf2,
//         schoolName: req.body.udf3,
//       });
//       res.redirect(
//         `https://www.celebrityschool.in/competition/thank-you?payment_status=Credited`
//       );
//     } else
//       res.redirect(
//         "https://www.celebrityschool.in/competition/thank-you?payment_status=failed"
//       );
//   } catch (error) {
//     failResponse(res, null, 401, error.message);
//   }
// };

exports.createCompetitionUser = async (req, res) => {
  try {
    let userId = req.user.id;
    let userEmail = req.user.email;
    const { name, albumId, age, city } = req.body;
    const user = await CompetetionUser.create({
      userId,
      albumId,
      userEmail,
      studentName: name,
      age,
      city,
    });

    sucessResponse(res, user, 200, "profile created ");
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

exports.updateCompetitionUser = async (req, res) => {
  try {
    let id = req.body.id;

    const user = await CompetetionUser.findById(id);

    user.isPaid = true;

    await user.save();

    sucessResponse(res, {}, 200, "user updated");
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

exports.getAllUsers = async (req, res) => {
  try {
    const users = await CompetetionUser.find({ isPaid: true });

    sucessResponse(
      res,
      { users, total: users.length },
      200,
      "check console..."
    );
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

function getCompetitionName(albumId) {
  if (albumId === 101) return "Singing Contest";
  if (albumId === 102) return "Digi Drawing Contest";
  if (albumId === 103) return "Story Telling Contest";
  if (albumId === 104) return "Slogan Contest";
  if (albumId === 105) return "Poetry Writing Contest";

  return albumId;
}

exports.usersJson = async (req, res) => {
  try {
    const { data } = await axios.get(
      "https://www.origin.celebrityschool.in:1337/api/order/competition-all"
    );

    let usersData = [];

    for (let i = 0; i < data.length; i++) {
      const users = await CompetetionUser.find({
        userEmail: data[i].email,
        albumId: data[i].ablum_id,
      }).sort({ createdAt: -1 });

      let user = users[0];

      if (user) {
        usersData.push({
          userId: user.userId,
          competition: getCompetitionName(+user.albumId),
          email: user.userEmail,
          studentName: user.studentName,
          schoolName: user.schoolName,
          location: user.location,
          standard: user.standard,
          createdAt: user.createdAt,
        });
      }
    }

    // data.forEach(async (userItem) => {
    //   const users = await CompetetionUser.find({
    //     userEmail: userItem.email,
    //   }).sort({ createdAt: -1 });

    //   let user = users[0];

    //   if (user) {
    //     usersData.push({
    //       userId: user.userId,
    //       competition: getCompetitionName(+user.albumId),
    //       email: user.userEmail,
    //       studentName: user.studentName,
    //       schoolName: user.schoolName,
    //       location: user.location,
    //       standard: user.standard,
    //       createdAt: user.createdAt,
    //     });
    //   }
    // });

    // const users = await CompetetionUser.find({ isPaid: true });

    // const usersData = users.map((user) => ({
    //   userId: user.userId,
    //   competition: getCompetitionName(+user.albumId),
    //   email: user.userEmail,
    //   studentName: user.studentName,
    //   schoolName: user.schoolName,
    //   location: user.location,
    //   standard: user.standard,
    //   createdAt: user.createdAt,
    // }));

    res.status(200).json(usersData);
    // sucessResponse(
    //   res,
    //   { users, total: users.length },
    //   200,
    //   "check console..."
    // );
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

exports.usersRegisterationJson = async (req, res) => {
  try {
    const { data } = await axios.get(
      "https://www.origin.celebrityschool.in:1337/api/order/competition-all-registerations"
    );

    let usersData = [];

    for (let i = 0; i < data.length; i++) {
      const users = await CompetetionUser.find({
        userEmail: data[i].email,
        albumId: data[i].ablum_id,
      }).sort({ createdAt: -1 });

      let user = users[0];

      usersData.push({
        id: data[i].id,
        competition: getCompetitionName(data[i].ablum_id),
        email: data[i].email,
        mobile: data[i].mobile,
        studentName: user.studentName,
        status: data[i].payment_status,
      });
    }

    // data.forEach(async (userItem) => {
    //   const users = await CompetetionUser.find({
    //     userEmail: userItem.email,
    //   }).sort({ createdAt: -1 });

    //   let user = users[0];

    //   if (user) {
    //     usersData.push({
    //       userId: user.userId,
    //       competition: getCompetitionName(+user.albumId),
    //       email: user.userEmail,
    //       studentName: user.studentName,
    //       schoolName: user.schoolName,
    //       location: user.location,
    //       standard: user.standard,
    //       createdAt: user.createdAt,
    //     });
    //   }
    // });

    // const users = await CompetetionUser.find({ isPaid: true });

    // const usersData = users.map((user) => ({
    //   userId: user.userId,
    //   competition: getCompetitionName(+user.albumId),
    //   email: user.userEmail,
    //   studentName: user.studentName,
    //   schoolName: user.schoolName,
    //   location: user.location,
    //   standard: user.standard,
    //   createdAt: user.createdAt,
    // }));

    res.status(200).json(usersData);
    // sucessResponse(
    //   res,
    //   { users, total: users.length },
    //   200,
    //   "check console..."
    // );
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};
