const UserGroup = require("../models/userGroup.model");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.addToGroup = async (req, res) => {
  try {
    const { groupId, userId } = req.body;
    let group;
    const existingGroup = await UserGroup.findOne({
      groupId,
    });
    if (!existingGroup) {
      group = await UserGroup.create({
        groupId,
        users: userId,
      });
      return sucessResponse(res, group, 200, "group created and user added");
    }

    group = await UserGroup.findByIdAndUpdate(
      existingGroup._id,
      {
        $addToSet: { users: userId },
      },
      { new: true }
    );
    return sucessResponse(res, group, 200, "user added sucessfully");
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};
