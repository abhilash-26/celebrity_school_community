const PushNotification = require("../models/pushNotification");
const { sucessResponse, failResponse } = require("../utils/responseHandler");
const axios = require("axios");
const cron = require("node-cron");
const csv = require("csv-express");
var serviceAccount = require("../../config/firbaseconfig.json");
const logger = require("../../config/logger");
const UserGroup = require("../models/userGroup.model");

exports.sendNotification = async (req, res) => {
  try {
    const { category, day, time } = req.body;
    let query;
    query = { day, time };
    let userList;
    let data;
    let dataAndroid;

    const notification = await PushNotification.findOne(query);
    if (!notification) return;
    if (notification.forUser != "all") {
      const result = await UserGroup.find({
        groupId: { $in: notification.userGroup },
      });
      let userDeviceIdArray = [];
      for (let i = 0; i < result.length; i++) {
        console.log(result[i].users);
        userDeviceIdArray = userDeviceIdArray.concat(result[i].users);
      }
      const userTokens = await axios({
        method: "post",
        url: "https://www.origin.celebrityschool.in:1337/api/v1/users/get-users-for-push-notification",
        data: {
          users: userDeviceIdArray,
        },
      });
      userList = userTokens.data.data;
      data = JSON.stringify({
        data: {
          notificationId: notification._id,
          category: notification.category,
          courseId: notification.courseId,
          mymediavideo: notification.image,
        },
        notification: {
          body: notification.message,
          title: notification.title,
          subtitle: notification.author,
        },
        mutable_content: true,
        registration_ids: userList,
      });
      dataAndroid = JSON.stringify({
        data: {
          notificationId: notification._id,
          title: notification.title,
          category: notification.category,
          courseId: notification.courseId,
          image: notification.image,
          message: notification.message,
          by: notification.author,
        },
        registration_ids: userList,
      });
    } else {
      data = JSON.stringify({
        data: {
          notificationId: notification._id,
          category: notification.category,
          courseId: notification.courseId,
          mymediavideo: notification.image,
        },
        notification: {
          body: notification.message,
          title: notification.title,
          subtitle: notification.author,
        },
        mutable_content: true,
        to: "/topics/com.IosCelebritySchool",
      });

      dataAndroid = JSON.stringify({
        data: {
          notificationId: notification._id,
          title: notification.title,
          category: notification.category,
          courseId: notification.courseId,
          image: notification.image,
          message: notification.message,
          by: notification.author,
        },
        to: "/topics/com.Celebrityschool",
        // to: "dkBVDw4WT6eV20Ex7bEQtO:APA91bGA2SNayHNJFbc-lqcviSnUL0gM3WIFNP9wf_d6x5Nr9Eh-mihwe-yAyMiUlPs6LRNgLamUav6477PxD6p6O_ABRDkPPWSqDgnkWPcfguW9gV5Li0IGRg5NwoBWGc6IrvHgTCGA",
      });
    }

    var config = {
      method: "post",
      url: "https://fcm.googleapis.com/fcm/send",
      headers: {
        "Content-Type": "application/json",
        Authorization: `key=${serviceAccount}`,
      },
      data: data,
    };

    var configAndroid = {
      method: "post",
      url: "https://fcm.googleapis.com/fcm/send",
      headers: {
        "Content-Type": "application/json",
        Authorization: `key=${serviceAccount}`,
      },
      data: dataAndroid,
    };

    const result = await axios(config);
    const result2 = await axios(configAndroid);
    updatePush(notification._id);
    const dataTosend = {
      res1: result.data,
      res2: result2.data,
    };
    sucessResponse(res, dataTosend, 200, "Notification sent");
  } catch (error) {
    logger.info(`error in send - ${error.message}`);
    failResponse(res, null, 400, error.message);
  }
};

exports.createNotification = async (req, res) => {
  try {
    const {
      message,
      category,
      day,
      author,
      title,
      image,
      courseId,
      time,
      userGroup,
      forUser,
    } = req.body;

    if (!category.trim() || !message.trim() || !image || !title.trim()) {
      return failResponse(res, null, 400, "Enter required fields");
    }
    if (category == "tips" && (!day.trim() || !author.trim() || !time.trim())) {
      return failResponse(res, null, 400, "Day author and time is required");
    }
    if (category == "course" && !courseId) {
      return failResponse(res, null, 400, "Course ID is required");
    }

    const result = await PushNotification.create({
      message,
      category,
      day,
      title,
      author,
      image,
      courseId,
      time,
      userGroup,
      forUser,
    });
    if (!result)
      return failResponse(
        res,
        null,
        400,
        "Something went wrong plesae try again later"
      );
    return sucessResponse(res, result, 200, "created notification");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.updateNotification = async (req, res) => {
  try {
    const { id, values } = req.body;

    for (const key in values) {
      if (!values[key].trim()) {
        return failResponse(res, null, 400, "Required fields can not be empty");
      }
    }

    const result = await PushNotification.findByIdAndUpdate(id, values, {
      new: true,
    });
    if (!result)
      return failResponse(
        res,
        null,
        400,
        "unable to update message please try again later"
      );
    return sucessResponse(res, result, 200, "updated message");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.updateViewCount = async (req, res) => {
  try {
    const { id, plateformType } = req.body;
    const userId = req.user.id;
    let viewCount = {
      userId,
      plateformType,
    };
    const result = await PushNotification.findByIdAndUpdate(
      id,
      {
        $addToSet: { viewCount: viewCount },
      },
      { new: true }
    );
    return sucessResponse(res, result, 200, "updated view count");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.removePushNotification = async (req, res) => {
  try {
    const { id } = req.body;
    const result = await PushNotification.findByIdAndRemove(id);
    if (!result)
      return failResponse(
        res,
        null,
        400,
        "unable to find message please try again later"
      );
    return sucessResponse(res, result, 200, "deleted message");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getAllPushNotification = async (req, res) => {
  try {
    let limit = req.query.limit;
    let searchvalue = req.query.searchvalue;
    let sortBy = req.query.sortBy || "";
    let sort = req.query.sort;
    let reg = "";
    if (searchvalue) {
      reg = searchvalue;
    }

    const allFilterSearch = {
      $or: [
        {
          author: {
            $regex: reg,
            $options: "i",
          },
        },
        {
          category: {
            $regex: reg,
            $options: "i",
          },
        },
      ],
    };
    let limitTo = parseInt(limit);
    const page = req.query.page;
    let skip = parseInt(limit) * (parseInt(page) - 1);
    let result = [];

    if (sortBy.trim() !== "")
      result = await PushNotification.find(allFilterSearch)
        .sort({
          [sortBy === "ID" ? "_id" : sortBy]: sort === "true" ? 1 : -1,
          sent_time: -1,
        })
        .limit(limitTo)
        .skip(skip);
    else
      result = await PushNotification.find(allFilterSearch)
        .limit(limitTo)
        .skip(skip);

    const count = await PushNotification.countDocuments(allFilterSearch);

    let dataTosend = result.map((item) => {
      return {
        _id: item._id,
        message: item.message,
        category: item.category,
        day: item.day,
        title: item.title,
        author: item.author,
        viewCount: item.viewCount.length,
      };
    });

    if (count === 0)
      dataTosend = [
        {
          _id: "",
          message: "",
          category: "",
          day: "",
          title: "",
          author: "",
        },
      ];

    let responseData = {
      data: dataTosend,
      count: count,
    };

    sucessResponse(res, responseData, 200, "web notifications are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.pushNotificationCsv = async (req, res) => {
  try {
    let limit = req.query.limit;
    let searchvalue = req.query.searchvalue;
    let sortBy = req.query.sortBy;
    let sort = req.query.sort;
    let reg = "";
    if (searchvalue) {
      reg = searchvalue;
    }

    const allFilterSearch = {
      $or: [
        {
          author: {
            $regex: reg,
            $options: "i",
          },
        },
        {
          category: {
            $regex: reg,
            $options: "i",
          },
        },
      ],
    };
    let limitTo = parseInt(limit);
    const page = req.query.page;
    let skip = parseInt(limit) * (parseInt(page) - 1);
    let result = [];

    if (sortBy && sortBy.trim() !== "")
      result = await PushNotification.find(allFilterSearch).sort({
        [sortBy === "ID" ? "_id" : sortBy]: sort === "true" ? 1 : -1,
      });
    else result = await PushNotification.find(allFilterSearch);

    // const count = await PushNotification.countDocuments(allFilterSearch);

    const fields = ["_id", "message", "author", "title", "category", "day"];

    const csvData = [fields];

    result.forEach((item) => {
      const row = [];

      fields.forEach((field) => {
        row.push(item[field]);
      });

      csvData.push(row);
    });

    res.csv(csvData);
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getSingleNotification = async (req, res) => {
  try {
    const { id } = req.body;
    if (!id) return failResponse(res, null, 400, "Id is required");
    const result = await PushNotification.findById(id);
    if (!result)
      return failResponse(
        res,
        null,
        400,
        "no notification found with given id"
      );
    return sucessResponse(res, result, 200, "Notifications are");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getNotificationTest = async (req, res) => {
  try {
    // console.log("h");
    // const result = await PushNotification.findOne({
    //   day: "29/5/2022",
    // });
    // if (result.userGroup.length > 0) {
    //   console.log("enter");
    // }
    const userList = [
      "eXiIOHpkQcuBWVvJUGoShY:APA91bGZFHFClETGzvavp1wffmsf_KA8wY7ZIPPjWcRsCaEDtIxQC4WTe327GrGAA-T9aG1g442zofDON2-WJ58RAqhWuwJD8tAjFC9gAuCdaGNvyARicK8XjzLWVA_n2T4ZP5Wsma0P",
      "ddE1gMYmSOWb5BS2qZLpGx:APA91bFmvmyjvIl4M5d2I19QdHaddQJdg_Mr3BuzCPTuW35IwV3nJyg4pjd6QZKCFZc0bJ8lxoOvUkxP8t3POExiUc7OTTr5Xd7AKAKvp23Q1c8HK8u_q8R5AiU2sWebyPklaDpHMDlt",
    ];
    dataAndroid = JSON.stringify({
      data: {
        notificationId: "733278",
        title: "test",
        category: "tips",
        courseId: "",
        image: "jijji",
        message: "test",
        by: "test",
      },
      registration_ids: userList,
    });
    var configAndroid = {
      method: "post",
      url: "https://fcm.googleapis.com/fcm/send",
      headers: {
        "Content-Type": "application/json",
        Authorization: `key=${serviceAccount}`,
      },
      data: dataAndroid,
    };

    // const result = await axios(config);
    const result2 = await axios(configAndroid);
    res.send(result2.data);
  } catch (error) {
    console.log(error.message);
  }
};

async function sendDailyTips(time) {
  try {
    const todayDate = new Date();
    const todayDay = todayDate.toLocaleDateString("en-IN");
    const result = await axios({
      method: "post",
      url: "https://chat.celebrityschool.in/v1/push-notification/send-push-notification",
      data: {
        category: "tips",
        day: todayDay,
        time: time,
      },
    });
  } catch (error) {
    logger.info(`${error.message}`);
  }
}

// cron.schedule(
//   "0 0 18 * * *",
//   () => {
//     sendDailyTips("evening");
//   },
//   {
//     scheduled: true,
//     timezone: "Asia/Kolkata",
//   }
// );

cron.schedule(
  "0 0 * * * *",
  () => {
    function formatAMPM(date) {
      var currentTime = new Date();
      var currentOffset = currentTime.getTimezoneOffset();
      var ISTOffset = 330; // IST offset UTC +5:30
      var ISTTime = new Date(
        currentTime.getTime() + (ISTOffset + currentOffset) * 60000
      );

      // ISTTime now represents the time in IST coordinates

      var hoursIST = ISTTime.getHours();
      var minutesIST = ISTTime.getMinutes();
      return `${hoursIST}`;
    }

    let todayDate = new Date();
    const todayTime = formatAMPM(todayDate);
    const todayDay = todayDate.toLocaleDateString("en-IN");
    logger.info(`day today - ${todayDay}`);
    logger.info(`time now - ${todayTime}`);
    sendDailyTips(todayTime);
  },
  {
    scheduled: true,
    timezone: "Asia/Kolkata",
  }
),
  // cron.schedule(
  //   "0 0 13 * * *",
  //   () => {
  //     sendDailyTips("afternoon");
  //   },
  //   {
  //     scheduled: true,
  //     timezone: "Asia/Kolkata",
  //   }
  // );

  // cron.schedule(
  //   "0 0 8 * * *",
  //   () => {
  //     sendDailyTips("morning");
  //   },
  //   {
  //     scheduled: true,
  //     timezone: "Asia/Kolkata",
  //   }
  // );

  (exports.getSentNotification = async (req, res) => {
    try {
      // const userId = 129566;
      // let albumIds = [];
      // const userGroup = await UserGroup.find({
      //   users: userId,
      // });
      // for (let i = 0; i < userGroup.length; i++) {
      //   albumIds.push(userGroup[i].groupId);
      // }
      // console.log(albumIds);
      const result = await PushNotification.find({
        sent: true,
        // userGroup: { $in: albumIds },
      }).sort({
        sent_time: -1,
      });
      return sucessResponse(res, result, 200, "Sent Notifications are");
    } catch (error) {
      failResponse(res, null, 400, error.message);
    }
  });

async function updatePush(id) {
  // var timeToExpire = new Date();
  // let getDate = timeToExpire.getDate();
  // timeToExpire.setDate(getDate + 7);
  const result = await PushNotification.findByIdAndUpdate(id, {
    sent_time: Date.now(),
    // expire_at: timeToExpire,
    sent: true,
  });
}
