const CertificateQuiz = require("../models/quiz.certificate.model");
const QuizScore = require("../models/certificate.quiz.score");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

// exports.createQuestion = async (req, res) => {
//   try {
//     // const { courseId, question, options, answer } = req.body;
//     const { ques } = req.body;
//     // console.log(req.body);

//     ques.forEach((item) => {
//       createData(item);
//     });

//     // const result = await CertificateQuiz.create({
//     //   courseId,
//     //   question,
//     //   options,
//     //   answer,
//     // });
//     sucessResponse(res, null, 200, "created");
//   } catch (error) {
//     failResponse(res, null, 400, error.message);
//   }
// };

exports.getQuestions = async (req, res) => {
  try {
    const { courseId } = req.query;
    const userEmail = req.user.email;
    const isTestTaken = await QuizScore.findOne({
      userEmail,
      courseId,
    });
    if (isTestTaken) {
      let data = {
        testTaken: true,
      };
      return sucessResponse(res, data, 200, "test taken");
    }
    const questions = await CertificateQuiz.find({
      courseId,
    }).select(["-answer", "-isActive"]);

    shuffleArray(questions);
    const dataToSend = {
      questions: questions,
      count: questions.length,
      testTaken: false,
    };
    sucessResponse(res, dataToSend, 200, "All questions");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.validateAnswer = async (req, res) => {
  try {
    const { courseId, questions } = req.body;
    const questionObj = {};
    questions.forEach((item) => {
      questionObj[item.question] = item.answer;
    });
    const tempQuestions = await CertificateQuiz.find({
      courseId,
    });
    let score = 0;
    tempQuestions.forEach((item) => {
      if (item.answer == questionObj[item._id]) {
        score++;
      }
    });
    const dataTosend = {
      score,
    };
    if (score < 10) {
      return sucessResponse(res, dataTosend, 200, "FAIL");
    }
    if (score >= 10) {
      const grade = createGrade(score);
      const result = await QuizScore.create({
        userEmail: req.user.email,
        courseId: courseId,
        score: score,
        grade: grade,
      });
    }

    return sucessResponse(res, dataTosend, 200, "PASS");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

// const createData = async (item) => {
//   const result = await CertificateQuiz.create({
//     courseId: item.courseId,
//     question: item.question,
//     options: item.options,
//     answer: item.answer,
//   });
// };

const createGrade = (marks) => {
  if (marks >= 20) return "platinum";
  if (marks >= 15) return "gold";
  if (marks >= 10) return "silver";
};
