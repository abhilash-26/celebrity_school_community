const UserCbo = require("../models/cbo.user.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { jwtSecret } = require("../../config/vars");
const axios = require("axios");

const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.userSignUp = async (req, res) => {
  const { token } = req.body;
  const {
    data: { success: recaptchaStatus },
  } = await axios.post(
    `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${token}`
  );

  if (recaptchaStatus === false)
    return failResponse(res, null, 400, "Robot 🤖");

  try {
    const { name, email, phone, password } = req.body;
    const tempUser = await UserCbo.findOne({ email });
    if (tempUser) {
      return failResponse(
        res,
        null,
        400,
        "User already registered with this email"
      );
    }
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, salt);
    let allPromocode = [];

    const promocode = await axios({
      method: "post",
      url: "https://www.origin.celebrityschool.in:1337/api/promocode/create-promocode-new",
      data: {
        applyOn: "all",
        promoName: `${name}-10-AC`,
        numberOf: 1,
        discount: 10,
        inventory: 999,
        assignedTo: email,
      },
    });
    // if (promocode?.data?.data) {
    if (promocode && promocode.data && promocode.data.data) {
      promocode.data.data.forEach((item) => {
        allPromocode.push(item);
      });
    }

    // const promocode2 = await axios({
    //   method: "post",
    //   url: "https://www.origin.celebrityschool.in:1337/api/promocode/create-promocode-new",
    //   data: {
    //     applyOn: "alllesson",
    //     promoName: `${name}-10-LE`,
    //     numberOf: 1,
    //     discount: 10,
    //     inventory: 999,
    //     assignedTo: email,
    //   },
    // });
    // if (promocode2?.data?.data) {
    //   promocode2.data.data.forEach((item) => {
    //     allPromocode.push(item);
    //   });
    // }

    const createdUser = await UserCbo.create({
      phone,
      email,
      promocodes: allPromocode,
      name,
      password: hashPassword,
    });

    const userTo = {
      email: createdUser.email,
      phone: createdUser.phone,
    };
    const accessToken = jwt.sign(userTo, jwtSecret);
    const dataToSend = {
      name: createdUser.name,
      phone: createdUser.phone,
      token: accessToken,
    };

    return sucessResponse(res, dataToSend, 200, "User registered successfully");
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};

/****************
  USER LOGIN
  ****************/

exports.userLogin = async (req, res) => {
  const { token } = req.body;
  const {
    data: { success: recaptchaStatus },
  } = await axios.post(
    `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${token}`
  );

  if (recaptchaStatus === false)
    return failResponse(res, null, 400, "Robot 🤖");

  try {
    const { password, email } = req.body;
    if (!password || !email) {
      return failResponse(res, null, 400, "Email and Password is required");
    }

    let user = await UserCbo.findOne({
      email,
    });

    if (!user) return failResponse(res, null, 400, "email not registered");

    const userTo = {
      email: email,
      phone: user.phone,
    };

    const accessToken = jwt.sign(userTo, jwtSecret);
    const dataToSend = {
      email: user.email,
      name: user.name,
      token: accessToken,
    };

    let validPin;
    let validPass;

    validPass = await bcrypt.compare(password, user.password);

    if (!validPass) {
      return failResponse(res, null, 400, "please check the login credentials");
    }

    return sucessResponse(res, dataToSend, 200, "User Logged in successfully");
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};

/****************
GET PROFILE
****************/

exports.getUserProfile = async (req, res) => {
  try {
    const email = req.user.email;
    const user = await UserCbo.findOne({ email }).select(["-password"]);
    if (!user) {
      return failResponse(res, null, 400, "No user found");
    }
    return sucessResponse(res, user, 200, "Profile fetched successfully");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
