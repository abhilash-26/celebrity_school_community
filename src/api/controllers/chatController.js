const Chat = require("../models/chatModel");
var Filter = require("bad-words");
filter = new Filter();
const logger = require("../../config/logger");
const axios = require("axios");
const { sucessResponse, failResponse } = require("../utils/responseHandler");
const UserBlock = require("../models/user.block.list");

const saveMessage = async ({
  userId,
  communityId,
  message,
  messageType,
  communityType,
  userName,
  userRole,
}) => {
  logger.info(` save here`);
  try {
    // Adding the time after which messages will be removed from db using ttl
    logger.info(
      ` from save message -- message-${message} userId ${userId} communityId ${communityId} userName ${userName} communityType ${communityType}`
    );
    var timeToExpire = new Date();

    // expiry of message set to 7 days after creation currently inactive

    // let getDate = timeToExpire.getDate();
    // timeToExpire.setDate(getDate + 7);

    // expiry of message set to one month after creation
    let getMonth = timeToExpire.getMonth();
    timeToExpire.setMonth(getMonth + 1);

    const userBlocked = await UserBlock.findOne({
      userId,
    });

    if (userBlocked) return "user blocked";

    // Filter out the bad words from message and replace it with *
    // let cleanMessage = filter.clean(message);
    // let messageToSave = cleanMessage.trim();
    if (userId == 122970) {
      return "Not allowed";
    }

    if ((message && communityId && userId && communityType && userName) == "") {
      return "field can't be empty";
    } else {
      const createdChatRoom = await Chat.create({
        userId: userId,
        userName: userName,
        userRole: userRole,
        communityId: communityId,
        communityType: communityType,
        message: message,
        messageType: messageType,
        expire_at: timeToExpire,
      });
      return createdChatRoom;
    }
  } catch (error) {
    console.log(error);
  }
};

const fetchMessage = async (communityId) => {
  try {
    // logger.info(`in fetch message from previous messages ${communityId}`);
    const prevMessage = await Chat.find({
      communityId: communityId,
    });

    if (prevMessage) return prevMessage;
    return {};
  } catch (error) {
    console.log(error);
  }
};

const sendPrevMessage = (result) => {
  return result.map((item) => item.message);
};

const fetchUser = async (email) => {
  try {
    console.log("inside fetch user");
    const user = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/user/fetch_user_profile",
      headers: {
        "x-auth-token": "aa",
        user_email: email,
      },
    });
    let userId = user.data.data.profile.id;
    let userName = user.data.data.profile.name;
    return { userId: userId, userName: userName };
  } catch (error) {
    return "unable to fetch user";
  }
};

const fetchLatestMessage = async (req, res) => {
  try {
    const lastmessage = await Chat.findOne({
      communityId: req.body.communityId,
    }).sort({ createdAt: -1 });
    if (!lastmessage) return failResponse(res, null, 400, "no message found");
    if (lastmessage.messageType == "image") {
      lastmessage.message = "Photo";
    }
    if (lastmessage.messageType == "video") {
      lastmessage.message = "Video";
    }
    sucessResponse(res, lastmessage, 200, "flag fetched are");
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};

const deleteMessage = async (messageId) => {
  try {
    const deletedMessage = await Chat.findByIdAndDelete(messageId);
    if (deletedMessage) return deletedMessage;
  } catch (error) {
    return "unable to delete message";
  }
};
module.exports = {
  saveMessage,
  fetchMessage,
  sendPrevMessage,
  fetchUser,
  fetchLatestMessage,
  deleteMessage,
};
