const Batch = require("../models/liveBatch");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.createBatch = async (req, res) => {
  try {
    const { batchName, startAt, endAt } = req.body;

    const [startDate, startMonth, startYear] = startAt.split("/");
    const [endDate, endMonth, endYear] = endAt.split("/");

    const start = new Date();
    start.setDate(startDate);
    start.setMonth(startMonth);
    start.setFullYear(startYear);

    const end = new Date();
    end.setDate(endDate);
    end.setMonth(endMonth);
    end.setFullYear(endYear);

    if (!batchName.trim()) {
      return failResponse(res, null, 400, "Batch name is required");
    }
    const tempBatch = await Batch.findOne({
      batchName,
    });
    if (tempBatch) {
      return failResponse(res, null, 400, "Batch name already registered");
    }

    const result = await Batch.create({
      batchName,
      startAt: start,
      endAt: end,
    });
    sucessResponse(res, result, 200, "Batch created sucessfully");
  } catch (error) {
    console.log(error);
    failResponse(res, null, 400, error.message);
  }
};

exports.adduserToBatch = async (req, res) => {
  try {
    const { userIds, id } = req.body;
    const result = await Batch.findByIdAndUpdate(
      id,
      {
        $addToSet: { usersEnrolled: userIds },
      },
      { new: true }
    );
    sucessResponse(res, result, 200, "User added");
  } catch (error) {
    console.log(error);
    failResponse(res, null, 400, error.message);
  }
};

exports.removeUserFromBatch = async (req, res) => {
  try {
    const { userIds, id } = req.body;
    const result = await Batch.findByIdAndUpdate(
      id,
      {
        $pullAll: { usersEnrolled: userIds },
      },
      { new: true }
    );
    sucessResponse(res, result, 200, "User remmoved");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.getAllBatch = async (req, res) => {
  try {
    let limit = req.query.limit;
    let searchvalue = req.query.searchvalue;
    let sortBy = req.query.sortBy;
    let sort = req.query.sort;
    let reg = "";
    if (searchvalue) {
      reg = searchvalue;
    }

    const allFilterSearch = {
      $or: [
        {
          batchName: {
            $regex: reg,
            $options: "i",
          },
        },
      ],
    };
    let limitTo = parseInt(limit);
    const page = req.query.page;
    let skip = parseInt(limit) * (parseInt(page) - 1);
    const result = await Batch.find(allFilterSearch)
      .sort({ startAt: -1 })
      .limit(limitTo)
      .skip(skip);

    const count = await Batch.countDocuments(allFilterSearch);
    let responseData = {
      data: result,
      count: count,
    };

    return sucessResponse(res, responseData, 200, "All batch fetched");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.deleteBatch = async (req, res) => {
  try {
    const { id } = req.body;
    const result = await Batch.findByIdAndDelete(id);
    if (result) {
      return sucessResponse(res, result, 200, "Batch deleted");
    }
    return failResponse(res, null, 400, "Something went wrong");
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};
