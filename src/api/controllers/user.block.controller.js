const UserBlock = require("../models/user.block.list");
const { failResponse, sucessResponse } = require("../utils/responseHandler");
const axios = require("axios");

exports.blockUser = async (req, res) => {
  try {
    const { userId } = req.body;
    const tempUser = await axios({
      method: "get",
      url: "https://www.origin.celebrityschool.in:1337/api/v1/user/fetch_user_profile",
      headers: {
        "x-auth-token": "asd",
        user_email: req.user.email,
      },
    });
    if (tempUser.data.data.profile.userRole != "admin") {
      return failResponse(res, null, 400, "Not allowed");
    }
    const tempBlocked = await UserBlock.findOne({
      userId,
    });
    if (tempBlocked) {
      // return failResponse(res, null, 400, "User already blocked");
      const unblocked = await UserBlock.findByIdAndDelete(tempBlocked._id);
      return sucessResponse(res, unblocked, 200, "User Unblocked");
    }
    const blocked = await UserBlock.create({
      userId,
    });
    return sucessResponse(res, blocked, 200, "User blocked");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};
