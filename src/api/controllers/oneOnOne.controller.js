const OneOnOne = require("../models/oneOnOne");
const Demo = require("../models/oneOnOneDemo");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.createUserDetail = async (req, res) => {
  try {
    const { userName, userEmail, instructor, startAt, endAt } = req.body;
    const userDetail = await OneOnOne.create({
      userName,
      userEmail,
      instructor,
      startAt,
      endAt,
    });
    sucessResponse(res, userDetail, 200, "User detail saved");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.createDemo = async (req, res) => {
  try {
    const userEmail = req.user.email;
    const userId = req.user.id;
    const { albumId } = req.body;
    const tempDemo = await Demo.findOne({
      userEmail,
      albumId,
    });
    if (tempDemo) {
      return failResponse(res, null, 400, "Demo already created for the user");
    }
    const createdDemo = await Demo.create({
      userEmail,
      userId,
      albumId,
    });
    sucessResponse(res, createdDemo, 200, "Demo recorded sucessfully");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.checkDemo = async (req, res) => {
  try {
    const { albumId, userEmail } = req.body;
    const tempDemo = await Demo.findOne({
      userEmail,
      albumId,
    });
    if (tempDemo) {
      return sucessResponse(res, tempDemo, 200, "Demo found");
    }
    return failResponse(res, null, 400, "No demo found");
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
};

exports.demoBooked = async (req, res) => {
  try {
    const { albumId } = req.query;
    let page = req.query.page || 1;
    const limit = parseInt(req.query.limit) || 10;
    const skip = (page - 1) * limit;
    const allDemo = await Demo.find({
      albumId,
    })
      .limit(limit)
      .skip(skip);
    if (allDemo.length == 0) {
      return failResponse(res, null, 400, "No demo found");
    }
    return sucessResponse(res, allDemo, 200, "All demo are");
  } catch (error) {
    return failResponse(res, null, 400, error.message);
  }
};
