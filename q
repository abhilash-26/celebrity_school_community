[33mcommit 9c5c0036327866d5457beaaea3e0176d8dbf73ab[m[33m ([m[1;36mHEAD -> [m[1;32mmain[m[33m, [m[1;31morigin/main[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Merge: 2a0297d 8045301
Author: jatinnegi <negijatinn@gmail.com>
Date:   Tue Aug 2 14:27:08 2022 +0530

    Merge branch 'main' of https://gitlab.com/abhilash-26/celebrity_school_community

[33mcommit 2a0297d004102734369530d989a344da847958a7[m[33m ([m[1;31morigin/development[m[33m, [m[1;32mdevelopment[m[33m)[m
Author: jatinnegi <negijatinn@gmail.com>
Date:   Tue Aug 2 14:26:37 2022 +0530

    Get Serum Institute users watch time data

[33mcommit 8045301d342d5dab7e88ccc6999cecec0038e1b4[m
Merge: 584b43f b8c648f
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Jul 29 09:19:46 2022 +0000

    Merge branch 'development' into 'main'
    
    view count
    
    See merge request abhilash-26/celebrity_school_community!114

[33mcommit b8c648f14bba0eddc704b23a8f310b401b954da8[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Jul 29 14:48:13 2022 +0530

    view count

[33mcommit 584b43fb42e8b482cbecbbd81f14734244538281[m
Merge: 2213a2a 75a17de
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jul 14 10:25:07 2022 +0000

    Merge branch 'development' into 'main'
    
    user tracking
    
    See merge request abhilash-26/celebrity_school_community!113

[33mcommit 75a17de0365f3a0833448f18fdfe1d3b802c42c6[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jul 14 15:53:44 2022 +0530

    user tracking

[33mcommit 2213a2a30d80286355feae8c1228ff3822de80b7[m
Merge: a2f2b94 3d6c813
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Jul 11 11:17:47 2022 +0000

    Merge branch 'development' into 'main'
    
    user questions
    
    See merge request abhilash-26/celebrity_school_community!112

[33mcommit 3d6c8138782d93e82bc076d0e27ce0f4ce0a4f28[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Jul 11 16:46:09 2022 +0530

    user questions

[33mcommit a2f2b944fc7f0f21b20c99740ab28965868da0bd[m
Merge: e28c315 89bca0f
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Jul 5 10:41:37 2022 +0000

    Merge branch 'development' into 'main'
    
    icons in userConfig
    
    See merge request abhilash-26/celebrity_school_community!111

[33mcommit 89bca0f7a040f4f762ec9abbb9e375ff1f173292[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Jul 5 16:10:49 2022 +0530

    icons in userConfig

[33mcommit e28c3156f9b8184761b61de9b3deeb10504f11ad[m
Merge: 6c1ac6a 97d0e97
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Jul 4 18:05:02 2022 +0000

    Merge branch 'development' into 'main'
    
    user config
    
    See merge request abhilash-26/celebrity_school_community!110

[33mcommit 97d0e977978fce6fd4db411d7f1df5dc9b9f71cd[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Jul 4 23:33:59 2022 +0530

    user config

[33mcommit 6c1ac6a303564e772b73ac03843e60b82b87cbf2[m
Merge: ae0e63d 82cab12
Author: jatinnegi <negijatinn@gmail.com>
Date:   Mon Jun 27 20:14:11 2022 +0530

    Merge branch 'development'

[33mcommit 82cab12e847cf580d5a985fe474310d677f19a43[m
Author: jatinnegi <negijatinn@gmail.com>
Date:   Mon Jun 27 20:13:50 2022 +0530

    Meeting hosts

[33mcommit ae0e63d42c679e915aa0733b0583e45dd9fd83fe[m
Merge: 2617908 d12157e
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat Jun 25 14:37:23 2022 +0000

    Merge branch 'development' into 'main'
    
    meeting changes
    
    See merge request abhilash-26/celebrity_school_community!109

[33mcommit d12157efea9c1fee31a79379920b22492785103b[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat Jun 25 20:04:47 2022 +0530

    meeting changes

[33mcommit 2617908a7cceb1f39b5c524e5a52e18768e961af[m
Merge: e21301b 6009e32
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Jun 6 08:40:19 2022 +0000

    Merge branch 'development' into 'main'
    
    live performance
    
    See merge request abhilash-26/celebrity_school_community!108

[33mcommit 6009e32a7077c508a8ba6ca8232687eb2b61d3c8[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Jun 6 14:08:42 2022 +0530

    live performance

[33mcommit e21301b02c9097d668d47e86787bf84fbc4003dd[m
Merge: 6c45f22 ce68f5c
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Jun 1 10:01:38 2022 +0000

    Merge branch 'development' into 'main'
    
    zoom meet changes
    
    See merge request abhilash-26/celebrity_school_community!107

[33mcommit ce68f5c5a348fc0a85f8199b166396d08b849ca8[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Jun 1 15:30:32 2022 +0530

    zoom meet changes

[33mcommit 6c45f221a0b519908e10680357d959f50f1968b8[m
Merge: b0b3a6e a0b40c0
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 31 18:48:24 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!106

[33mcommit a0b40c0b3463c1594a4e5c6aa3a2c537a8fd670a[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Jun 1 00:17:20 2022 +0530

    minor fix

[33mcommit b0b3a6edd044a8649b3410541600adb142e3b7a5[m
Merge: 3d0f709 99671cf
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 31 10:25:34 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!105

[33mcommit 99671cfcc97a37ae8ec2df2186daa509863c2b5b[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 31 15:54:37 2022 +0530

    minor fix

[33mcommit 3d0f709c47fdd60efa12a4213618fb7f39c97ee4[m
Merge: e738b69 e3e7f8a
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 31 08:56:20 2022 +0000

    Merge branch 'development' into 'main'
    
    Development
    
    See merge request abhilash-26/celebrity_school_community!104

[33mcommit e3e7f8a64b335487c96e43a22c8a5e7ccd1c35cd[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 31 14:24:16 2022 +0530

    meet type

[33mcommit 547ef3f4b4b232eb200501db2022842ac56d24da[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 31 14:20:24 2022 +0530

    meet zoom

[33mcommit e738b69426b1c87370d70a52918ca30d5a54c1d2[m
Merge: 202863a 8540354
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat May 28 09:46:42 2022 +0000

    Merge branch 'development' into 'main'
    
    push notification intent creation
    
    See merge request abhilash-26/celebrity_school_community!103

[33mcommit 8540354c2b7053c72c9b9996ab7651d8691bc3cd[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat May 28 15:15:07 2022 +0530

    push notification intent creation

[33mcommit 202863a5d927aabde399857d3d747487b8514230[m
Merge: 9dec55e 6d8b6cf
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu May 26 05:19:58 2022 +0000

    Merge branch 'development' into 'main'
    
    user group
    
    See merge request abhilash-26/celebrity_school_community!102

[33mcommit 6d8b6cfd8a72ee4b220b1dace40e6e5def554f39[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu May 26 10:48:10 2022 +0530

    user group

[33mcommit 9dec55ea4413c97c45452f1effac050b65f8c231[m
Merge: fa9d814 fde94f3
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 10:44:30 2022 +0000

    Merge branch 'development' into 'main'
    
    logger chk
    
    See merge request abhilash-26/celebrity_school_community!101

[33mcommit fde94f30bde007ce149f65be0a56df58299e3b93[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 16:13:40 2022 +0530

    logger chk

[33mcommit fa9d8149b93df7f91f61799908268ccff3a390c2[m
Merge: 4cb1e25 9980dc6
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 10:39:20 2022 +0000

    Merge branch 'development' into 'main'
    
    filter check
    
    See merge request abhilash-26/celebrity_school_community!100

[33mcommit 9980dc61d28b31025f73cddd5e4860766ff2ecc9[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 16:08:09 2022 +0530

    filter check

[33mcommit 4cb1e25f6cd3f34cce2bc3968eade202e74a494e[m
Merge: 06ccbe8 3bc4ffa
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 10:15:44 2022 +0000

    Merge branch 'development' into 'main'
    
    logger check
    
    See merge request abhilash-26/celebrity_school_community!99

[33mcommit 3bc4ffae2da7534720d9145ec682a0957ac69e77[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 15:44:34 2022 +0530

    logger check

[33mcommit 06ccbe8cfbb388b806d2603a28e2b4d08bb5ef77[m
Merge: 1e0c9ae 486cb63
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 08:18:12 2022 +0000

    Merge branch 'development' into 'main'
    
    logger chk
    
    See merge request abhilash-26/celebrity_school_community!98

[33mcommit 486cb6398c567a2c5c21da42a230de5e6b05f395[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 13:47:12 2022 +0530

    logger chk

[33mcommit 1e0c9aec0eadd9a54260d0cac3cc948d646fe09a[m
Merge: 09c26cd a8ccaec
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 08:06:37 2022 +0000

    Merge branch 'development' into 'main'
    
    logger change
    
    See merge request abhilash-26/celebrity_school_community!97

[33mcommit a8ccaec12815486f5754177be0a27facfc321759[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 23 13:34:58 2022 +0530

    logger change

[33mcommit 09c26cd7972d735a74d4b093849f36761becd291[m
Merge: 2e404eb 7d5f2a9
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 10 12:45:54 2022 +0000

    Merge branch 'development' into 'main'
    
    new admin added
    
    See merge request abhilash-26/celebrity_school_community!96

[33mcommit 7d5f2a9f9dc582197c740f03b6ba42fe6d50c037[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 10 18:14:24 2022 +0530

    new admin added

[33mcommit 2e404ebf628d5861cedac7d94a3add7aadb67751[m
Merge: 62d9596 c1083d8
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 10 10:07:17 2022 +0000

    Merge branch 'development' into 'main'
    
    role based on groups
    
    See merge request abhilash-26/celebrity_school_community!95

[33mcommit c1083d8b0b8e52e153f4f9ba306cecb1eb7076f2[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 10 15:32:30 2022 +0530

    role based on groups

[33mcommit 62d95960cbd0da6dc88b4fd25989d47d39873f4c[m
Merge: 795a544 6f1e961
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 3 07:23:20 2022 +0000

    Merge branch 'development' into 'main'
    
    userRole
    
    See merge request abhilash-26/celebrity_school_community!94

[33mcommit 6f1e96132d37bb6cb96dc0658801514de6027eb6[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue May 3 12:49:05 2022 +0530

    userRole

[33mcommit 795a544da2ec79dbd4b429f38444f70ee843253c[m
Merge: 12bdb0a b704b15
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 2 11:56:38 2022 +0000

    Merge branch 'development' into 'main'
    
    order change meeting and push
    
    See merge request abhilash-26/celebrity_school_community!93

[33mcommit b704b15f08af11eedd07e35937ddb1ca8ec06a0f[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon May 2 17:22:18 2022 +0530

    order change meeting and push

[33mcommit 12bdb0a778cf3ce65ee6514d979047aa73cd209f[m
Merge: 7776aaa 5f5ebc5
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 28 10:25:25 2022 +0000

    Merge branch 'development' into 'main'
    
    meeting type
    
    See merge request abhilash-26/celebrity_school_community!92

[33mcommit 5f5ebc5efe69cd7c9caaf830a5e70c871ff5fd86[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 28 15:51:51 2022 +0530

    meeting type

[33mcommit 7776aaa8fa0270bd6f0c70878eef97ac99298b2f[m
Merge: cf73379 ac888a2
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Apr 27 07:38:01 2022 +0000

    Merge branch 'development' into 'main'
    
    course type notification minor change
    
    See merge request abhilash-26/celebrity_school_community!91

[33mcommit ac888a2ff8030f936264de89e82457b89dc04692[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Apr 27 13:06:29 2022 +0530

    course type notification minor change

[33mcommit cf733795d517c77a570d9a3a2f0032a8a74b335d[m
Merge: 4cf27c3 3dba51c
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 22 14:44:57 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix chat
    
    See merge request abhilash-26/celebrity_school_community!90

[33mcommit 3dba51cc6359f23ef8411a02b4b5a58db5223735[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 22 20:13:29 2022 +0530

    minor fix chat

[33mcommit 4cf27c320cf1664808004bc912254de268cf55fd[m
Merge: 201212d bd9fb7c
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 22 05:03:14 2022 +0000

    Merge branch 'development' into 'main'
    
    sent message no expiry
    
    See merge request abhilash-26/celebrity_school_community!89

[33mcommit bd9fb7cd2bad46384678dadc94ee8f8ca5f9a17e[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 22 10:31:13 2022 +0530

    sent message no expiry

[33mcommit 201212d6c4dc98548fe79b908bdb91cd168da36f[m
Merge: 9537c6b bf33840
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 21 05:07:32 2022 +0000

    Merge branch 'development' into 'main'
    
    hitachi video progress
    
    See merge request abhilash-26/celebrity_school_community!88

[33mcommit bf33840284a46bbe373cbae44b3081c945c2c341[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 21 10:32:11 2022 +0530

    hitachi video progress

[33mcommit 9537c6b26b81971391d1385e2408ef1d3db490af[m
Merge: ae64d76 6edbde7
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 15 14:51:25 2022 +0000

    Merge branch 'development' into 'main'
    
    Development
    
    See merge request abhilash-26/celebrity_school_community!87

[33mcommit 6edbde7b798b024e8986d9abe300f2fdd40a9c01[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 15 20:19:47 2022 +0530

    minor fix

[33mcommit 9886ccda1b2f10c30ffdffab493c678d7883063b[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 15 20:02:06 2022 +0530

    per course watch time

[33mcommit ae64d7604707bf797970bd8db894cd409d66daff[m
Merge: b284ca1 061cbae
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 15 07:36:54 2022 +0000

    Merge branch 'development' into 'main'
    
    message delete minor changes
    
    See merge request abhilash-26/celebrity_school_community!86

[33mcommit 061cbae10c1a21e1f25506d1f86743d557210137[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 15 13:05:02 2022 +0530

    message delete minor changes

[33mcommit b284ca19c9dd670776c0ff93c9be58ec013edfd4[m
Merge: b0ea288 b1f4111
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 14 15:04:40 2022 +0000

    Merge branch 'development' into 'main'
    
    delete event chat
    
    See merge request abhilash-26/celebrity_school_community!85

[33mcommit b1f41113cf77a2cbb2c76056cea47e240ed576c5[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 14 20:32:17 2022 +0530

    delete event chat

[33mcommit b0ea288a67602e79fe7a6b7881142c91aa1502d1[m
Merge: d117d47 bfb4555
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 14 10:30:19 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix all track api
    
    See merge request abhilash-26/celebrity_school_community!84

[33mcommit bfb45559e5fad70e762fcc270d21492616b6206f[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 14 15:58:56 2022 +0530

    minor fix all track api

[33mcommit d117d47ee127b3eb416d062c24f14036f8ab3a3e[m
Merge: ee45887 b762696
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 14 08:34:41 2022 +0000

    Merge branch 'development' into 'main'
    
    indivisual track
    
    See merge request abhilash-26/celebrity_school_community!83

[33mcommit b7626967883fa9f58d357d7b9baacf6e9871e1be[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 14 14:03:20 2022 +0530

    indivisual track

[33mcommit ee458875cb3c02fdeafbc6bd3a0905d9a4140497[m
Merge: 68d7e89 629f71f
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 14 05:38:30 2022 +0000

    Merge branch 'development' into 'main'
    
    video track user indivisual lesson
    
    See merge request abhilash-26/celebrity_school_community!82

[33mcommit 629f71fb175a6d9bd15a81dce014d57f5d1fc4a2[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 14 11:06:35 2022 +0530

    video track user indivisual lesson

[33mcommit 68d7e89dfbf230d85e98f5be9724559881168e34[m
Merge: 7ce36ca 5bc2ca2
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Apr 13 12:21:50 2022 +0000

    Merge branch 'development' into 'main'
    
    video tracking stat
    
    See merge request abhilash-26/celebrity_school_community!81

[33mcommit 5bc2ca252b48e43774cf9d9565c6e4656966e09a[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Apr 13 17:50:45 2022 +0530

    video tracking stat

[33mcommit 7ce36cad3ae7413466dca3a7ca404a607bd4920c[m
Merge: 76b4f4b da921de
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Apr 11 10:00:57 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!80

[33mcommit da921decf98e0622b536ffd8c078ee0188bdc106[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Apr 11 15:29:46 2022 +0530

    minor fix

[33mcommit 76b4f4b17cf0681dacbbc71307108a369dacd25b[m
Merge: 4631c02 232ee27
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Apr 11 09:51:15 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!79

[33mcommit 232ee27eb6d20fcebad54e0d25eeb8b3c057726f[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Apr 11 15:20:27 2022 +0530

    minor fix

[33mcommit 4631c02e723407cf2e59014cdaddf78f9b0aec30[m
Merge: 6606df6 041d269
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Apr 11 09:43:16 2022 +0000

    Merge branch 'development' into 'main'
    
    certificate download
    
    See merge request abhilash-26/celebrity_school_community!78

[33mcommit 041d26945a0006ec03e4fb5a433b0ddbeb15f896[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Apr 11 14:49:00 2022 +0530

    certificate download

[33mcommit 6606df6f16ba1f0e3fc3a3aaa01d99c6a615b514[m
Merge: e03c393 bb00e76
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 7 13:02:01 2022 +0000

    Merge branch 'development' into 'main'
    
    remove meeting api
    
    See merge request abhilash-26/celebrity_school_community!77

[33mcommit bb00e76774e190b585d7c8c3db996d558d06818f[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 7 18:31:01 2022 +0530

    remove meeting api

[33mcommit e03c39375f2bfa456362c2aa480e570965f4cc05[m
Merge: fa5aa3e 96341e1
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 7 06:22:18 2022 +0000

    Merge branch 'development' into 'main'
    
    video tracking
    
    See merge request abhilash-26/celebrity_school_community!76

[33mcommit 96341e11f865fe473ee0ae1110a6fb489ce1d1bf[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Apr 7 11:50:47 2022 +0530

    video tracking

[33mcommit fa5aa3e6e4912aeb207110761bf651c3cbf22256[m
Merge: db4a190 e499c74
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Apr 5 14:16:00 2022 +0000

    Merge branch 'development' into 'main'
    
    chat image video changes
    
    See merge request abhilash-26/celebrity_school_community!75

[33mcommit e499c742489bde4eac084240dc5f7f3b90e28fd7[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Apr 5 19:44:17 2022 +0530

    chat image video changes

[33mcommit db4a190f6a618fa22b3763148ab84e3b5d1916ee[m
Author: jatinnegi <negijatinn@gmail.com>
Date:   Tue Apr 5 10:49:12 2022 +0530

    Add comment model

[33mcommit 72252c2fdf6def522703eae4dde5c8fa59b06382[m
Author: jatinnegi <negijatinn@gmail.com>
Date:   Sat Apr 2 05:12:22 2022 +0530

    Changes in update web notification

[33mcommit 6a7f3a7fc86e7089babe2bbb008a6b535a5bb5c6[m
Merge: 815ccad 5901b11
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 1 12:07:49 2022 +0000

    Merge branch 'development' into 'main'
    
    ios notification
    
    See merge request abhilash-26/celebrity_school_community!74

[33mcommit 5901b11fc0b774535172fae12c61eb3ffb68fc5e[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 1 17:36:32 2022 +0530

    ios notification

[33mcommit 815ccad83d512bbe899a77ff1a438b3510f28fb9[m
Merge: 23eabce 26793e6
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 1 05:28:08 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!73

[33mcommit 26793e6c15f373f4dac4c8dfd1a05965de80722a[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 1 10:56:47 2022 +0530

    minor fix

[33mcommit 23eabce6277d979d5e7fa00ce3144b417456edb6[m
Merge: feb742e 099cafd
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 1 05:06:25 2022 +0000

    Merge branch 'development' into 'main'
    
    certificate download minor change
    
    See merge request abhilash-26/celebrity_school_community!72

[33mcommit 099cafdd39a7203ada259b0c4ba095685e3e11e5[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 1 10:33:14 2022 +0530

    certificate download minor change

[33mcommit feb742ebed81e95506555d93a86556896514ba41[m
Merge: 6723d5f 941f68f
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 1 04:17:54 2022 +0000

    Merge branch 'development' into 'main'
    
    certificate download count
    
    See merge request abhilash-26/celebrity_school_community!71

[33mcommit 941f68f98e92a760d1ed09558cd249626e78e22c[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Apr 1 09:43:05 2022 +0530

    certificate download count

[33mcommit 6723d5fd23084d4659abe304a2311da0150cd5e3[m
Merge: 960cbc3 a286b9d
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 31 17:57:03 2022 +0000

    Merge branch 'development' into 'main'
    
    certificate Download track
    
    See merge request abhilash-26/celebrity_school_community!70

[33mcommit a286b9dbbd2eb5301e170cc06064c0051da742ef[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 31 23:23:52 2022 +0530

    certificate Download track

[33mcommit 960cbc36635c624a77b2648a5f0d972af5c992a3[m
Merge: 00fc6f7 38b2c16
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Mar 29 07:08:02 2022 +0000

    Merge branch 'development' into 'main'
    
    view count notification
    
    See merge request abhilash-26/celebrity_school_community!69

[33mcommit 38b2c160fc7116cb5a180bbaae470fe0e2c8cac4[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Mar 29 12:35:15 2022 +0530

    view count notification

[33mcommit 00fc6f7262b5631e58b6513b1b8ebfa5c8dcdfe0[m
Merge: 5570edf a2a48ca
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 25 08:24:00 2022 +0000

    Merge branch 'development' into 'main'
    
    webinar new changes
    
    See merge request abhilash-26/celebrity_school_community!68

[33mcommit a2a48ca139d5bdb1fa8988ff4487c8d74c16fb24[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 25 13:43:47 2022 +0530

    webinar new changes

[33mcommit 5570edf292ebd3306bf7e7d0c076dde172624f2c[m
Merge: 0f3f76e 3a3ea61
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 24 18:33:15 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!67

[33mcommit 3a3ea613c2b59f4393040d1133d36a9ab9bb4699[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 25 00:00:32 2022 +0530

    minor fix

[33mcommit 0f3f76eca256b9b4419f4e6c3a6045f71cb060c4[m
Merge: e944b03 faa17ef
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 23 07:48:31 2022 +0000

    Merge branch 'development' into 'main'
    
    s3 url
    
    See merge request abhilash-26/celebrity_school_community!66

[33mcommit faa17ef47f925efea839ff65b9253369bb633448[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 23 13:16:55 2022 +0530

    s3 url

[33mcommit e944b034c5618d4fe06cf32c068085833bd0e046[m
Merge: 6c15a1e 6e2ce1c
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Mar 22 13:53:54 2022 +0000

    Merge branch 'development' into 'main'
    
    host profile
    
    See merge request abhilash-26/celebrity_school_community!65

[33mcommit 6e2ce1c7c74740546056e9805411b0ed0192f7e8[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Mar 22 19:20:00 2022 +0530

    host profile

[33mcommit 6c15a1e21fd541b4a3998064461e4af6baf38c83[m
Merge: 1635adb 53421dd
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Mar 21 10:37:41 2022 +0000

    Merge branch 'development' into 'main'
    
    preset
    
    See merge request abhilash-26/celebrity_school_community!64

[33mcommit 53421dda88011dd3a1cba54d64a44e2700a60e68[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Mar 21 16:05:07 2022 +0530

    preset

[33mcommit 1635adb38b75d14c89ea9964fe5691ded79d3ac3[m
Merge: 2ead6c3 19fe5b0
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 16 11:31:42 2022 +0000

    Merge branch 'development' into 'main'
    
    get single meeting
    
    See merge request abhilash-26/celebrity_school_community!63

[33mcommit 19fe5b0d7deb40c78fe7ae996cbeca9a4ec4088a[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 16 17:00:24 2022 +0530

    get single meeting

[33mcommit 2ead6c354f17bd338948a938a02949e0b9f7b468[m
Merge: 9443bd0 073a452
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 16 08:19:09 2022 +0000

    Merge branch 'development' into 'main'
    
    get
    
    See merge request abhilash-26/celebrity_school_community!62

[33mcommit 073a452ac2b4137ac0389896551715dc2112559f[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 16 13:47:36 2022 +0530

    get

[33mcommit 9443bd0e4defcfe81cff5e87d23eb00ead6def0b[m
Merge: 1604df0 b3f5b14
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 16 07:18:25 2022 +0000

    Merge branch 'development' into 'main'
    
    meeting changes
    
    See merge request abhilash-26/celebrity_school_community!61

[33mcommit b3f5b141b287fc2733539d856e0c83dbda5d3783[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 16 12:45:28 2022 +0530

    meeting changes

[33mcommit 1604df0bf5a40241b7bf35c5645b7ae084697d09[m
Merge: 7538fc4 4454176
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Mar 14 03:35:17 2022 +0000

    Merge branch 'development' into 'main'
    
    expiry time modify
    
    See merge request abhilash-26/celebrity_school_community!60

[33mcommit 4454176069ec771554ae71b4491ba1b3a25fa476[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Mar 14 09:03:09 2022 +0530

    expiry time modify

[33mcommit 7538fc454ea74dd60fc3c1bf838da6eebc8f082f[m
Merge: 977b993 b8b090e
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat Mar 12 08:26:54 2022 +0000

    Merge branch 'development' into 'main'
    
    web notification
    
    See merge request abhilash-26/celebrity_school_community!59

[33mcommit b8b090ed64a9ba1be54f96a0de52e9858aacd817[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat Mar 12 13:55:02 2022 +0530

    web notification

[33mcommit 977b993f607a2d1ae50116f338d4e0d3157c5b62[m
Merge: cb8710d 434620e
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 10:20:19 2022 +0000

    Merge branch 'development' into 'main'
    
    timing
    
    See merge request abhilash-26/celebrity_school_community!58

[33mcommit 434620eaa87f8886c6b297212539add976126dcd[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 15:49:02 2022 +0530

    timing

[33mcommit cb8710d0f26b2c72dc77a2f0872a0a0077faa762[m
Merge: 9e86b98 ebfafe8
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 09:42:29 2022 +0000

    Merge branch 'development' into 'main'
    
    time format
    
    See merge request abhilash-26/celebrity_school_community!57

[33mcommit ebfafe8561a79389b086037216f76aaf840cab59[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 15:11:40 2022 +0530

    time format

[33mcommit 9e86b98eb191d9922093ad5dfabd5727f60b76ca[m
Merge: 4844d97 539c6f8
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 08:04:09 2022 +0000

    Merge branch 'development' into 'main'
    
    timing fix
    
    See merge request abhilash-26/celebrity_school_community!56

[33mcommit 539c6f8bd6a8c2ceac8c376c778c5a41023299f6[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 13:33:28 2022 +0530

    timing fix

[33mcommit 4844d97680278ba3d739f9451f1f7dfc3792973b[m
Merge: 6e978b4 283510b
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 07:46:22 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!55

[33mcommit 283510be67d9a49a33cb9b10f5b0ae9a760a00ec[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 13:15:15 2022 +0530

    minor fix

[33mcommit 6e978b4dfcf9c44b2f59d86c13930291775cbced[m
Merge: ae3c2a1 d475b9d
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 07:27:10 2022 +0000

    Merge branch 'development' into 'main'
    
    time format
    
    See merge request abhilash-26/celebrity_school_community!54

[33mcommit d475b9d9fdf7c1ca853346e60d12b6fc696e30f1[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 12:56:27 2022 +0530

    time format

[33mcommit ae3c2a1e79c4e132d438c5c2469e0341cd635adf[m
Merge: e1d4a55 6406441
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 07:04:29 2022 +0000

    Merge branch 'development' into 'main'
    
    time test
    
    See merge request abhilash-26/celebrity_school_community!53

[33mcommit 640644110f1dab0dbb91a72008941fec8fec7912[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 12:33:42 2022 +0530

    time test

[33mcommit e1d4a55c2c7047e556cc46605e76ad756ca6e81d[m
Merge: 71c2867 0455a98
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 06:46:22 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!52

[33mcommit 0455a9866d0191725cc11685651dcfb1f78cb682[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 12:15:30 2022 +0530

    minor fix

[33mcommit 71c286789e817e74b249130ff64776a51736ce94[m
Merge: d87662a dcbcc92
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 06:25:05 2022 +0000

    Merge branch 'development' into 'main'
    
    fix
    
    See merge request abhilash-26/celebrity_school_community!51

[33mcommit dcbcc92e6dfd0fca35f5b1f88478ac40604dcbea[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 11:54:13 2022 +0530

    fix

[33mcommit d87662a541dac3845fd19f38e341e4f6547e23e7[m
Merge: bb8dd0d b8c6cce
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 06:11:55 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!50

[33mcommit b8c6cce5417df91ec52eeccaaca714ae119257b1[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 11:41:01 2022 +0530

    minor fix

[33mcommit bb8dd0d14d105acc363b0f0235b083bcc52b0881[m
Merge: ba7ba8e 0313d17
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 05:45:54 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!49

[33mcommit 0313d178448d45a825b7b91d177ae41de0dfc6d4[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 11 11:14:56 2022 +0530

    minor fix

[33mcommit ba7ba8efd0f8e769c5f0f4db9ab72b61b9ca5156[m
Merge: 16c26d2 d0b46d5
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 18:08:20 2022 +0000

    Merge branch 'development' into 'main'
    
    minor fix
    
    See merge request abhilash-26/celebrity_school_community!48

[33mcommit d0b46d52e2d263fdf5637d38148781c5e0aa8fce[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 23:37:24 2022 +0530

    minor fix

[33mcommit 16c26d225d2285792b5a53796d41aefa6199ffe4[m
Merge: 7c39e28 1c9a042
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 13:41:12 2022 +0000

    Merge branch 'development' into 'main'
    
    notification
    
    See merge request abhilash-26/celebrity_school_community!47

[33mcommit 1c9a04294d20543da21d02a735bc9566493f830b[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 19:09:55 2022 +0530

    notification

[33mcommit 7c39e283129e0e4ad873b09ca9e19112243b5fcc[m
Merge: 10eae26 18e5839
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 08:28:15 2022 +0000

    Merge branch 'development' into 'main'
    
    timing minor change
    
    See merge request abhilash-26/celebrity_school_community!46

[33mcommit 18e5839ed66b63390b48f3f014ad14c62fcec29f[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 13:57:20 2022 +0530

    timing minor change

[33mcommit 10eae2676e81d699d5674967a63dfc4edf524302[m
Merge: bca6502 4703900
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 08:19:54 2022 +0000

    Merge branch 'development' into 'main'
    
    notification date change
    
    See merge request abhilash-26/celebrity_school_community!45

[33mcommit 4703900fa533f3374d260888e016fe7fa0d387a5[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 13:48:44 2022 +0530

    notification date change

[33mcommit bca6502c1db39a9624d2139760eca4c198cfc2d3[m
Merge: dbc9708 e2988fd
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 07:39:15 2022 +0000

    Merge branch 'development' into 'main'
    
    blog minor change
    
    See merge request abhilash-26/celebrity_school_community!44

[33mcommit e2988fd173faaa50d061458645e0b5b3da82cdd5[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 13:07:04 2022 +0530

    blog minor change

[33mcommit dbc9708b8016a478c1e4e997ee3671f9a589440c[m
Merge: 75efd1c 013b073
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 07:08:18 2022 +0000

    Merge branch 'development' into 'main'
    
    blog user
    
    See merge request abhilash-26/celebrity_school_community!43

[33mcommit 013b0736a4d244ec89d0543118b76565cdf3a04d[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 10 12:36:46 2022 +0530

    blog user

[33mcommit 75efd1c630c39a2c1eb86645e2d5b76e277520ad[m
Merge: 5649429 8098735
Author: negijatinn <negijatinn@gmail.com>
Date:   Mon Mar 7 12:19:28 2022 +0530

    Merge branch 'main' of https://gitlab.com/abhilash-26/celebrity_school_community

[33mcommit 564942999be2764e6a6e9460fe1edd5afe7b1a2d[m
Merge: 4a07c60 584495d
Author: negijatinn <negijatinn@gmail.com>
Date:   Mon Mar 7 12:18:14 2022 +0530

    Merge branch 'development'

[33mcommit 584495da43127c57ba085c3a9c3e4b4b725e190b[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Mon Mar 7 12:17:53 2022 +0530

    Push notifications Sorting, CSV, and Validation

[33mcommit 8098735c82b6f491dc0b341bea5d09355e5e8f83[m
Merge: 4a07c60 8bce99a
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Mar 7 06:37:27 2022 +0000

    Merge branch 'development' into 'main'
    
    notification count
    
    See merge request abhilash-26/celebrity_school_community!42

[33mcommit 8bce99aecdebba7db9aa623f030f04deafe37617[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Mar 7 12:06:25 2022 +0530

    notification count

[33mcommit 4a07c6049430935745d39a74ab292faf8f7275e8[m
Merge: d97b959 c468cb1
Author: negijatinn <negijatinn@gmail.com>
Date:   Mon Mar 7 03:20:24 2022 +0530

    Merge branch 'development'

[33mcommit c468cb15f2493dd9f75e4c2e3a9fe77a09741c98[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Mon Mar 7 03:20:12 2022 +0530

    Push Notifications Sorting, CSV and Validation

[33mcommit d97b959ddec7f72d3241bfc3de47eeb41996a9d9[m
Merge: ca63ead abb6fbe
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 4 16:56:21 2022 +0000

    Merge branch 'development' into 'main'
    
    message trim
    
    See merge request abhilash-26/celebrity_school_community!41

[33mcommit abb6fbef58480d6a519989b995ce23d936ea835d[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 4 22:24:35 2022 +0530

    message trim

[33mcommit ca63eadda00fc1a625d10344d30bc478d4b7c2b4[m
Merge: 7f56643 bdebc6b
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 4 12:33:10 2022 +0000

    Merge branch 'development' into 'main'
    
    Development
    
    See merge request abhilash-26/celebrity_school_community!40

[33mcommit bdebc6b2f56f6a28913d3f11568c0821e28662ce[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 4 18:02:15 2022 +0530

    crud push notification

[33mcommit d5aedbb50a14e0e71dd89846656ebf8dc409a6f0[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Mar 4 14:14:39 2022 +0530

    count

[33mcommit 7f566436b3e59f4da12332e921a93a7dfb1b22ce[m
Merge: ad5647a f110d9f
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 13:05:54 2022 +0000

    Merge branch 'development' into 'main'
    
    minor change
    
    See merge request abhilash-26/celebrity_school_community!39

[33mcommit f110d9f8420351c269a5fb3e3fbb81461798aa3b[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 18:34:57 2022 +0530

    minor change

[33mcommit ad5647a4f81c0aef144163c1be816635e7511bd0[m
Merge: 9b703e8 4701a77
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 13:00:20 2022 +0000

    Merge branch 'development' into 'main'
    
    sent notification
    
    See merge request abhilash-26/celebrity_school_community!38

[33mcommit 4701a777fd8029caba2bb5015287bd9ddb1169b5[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 18:27:03 2022 +0530

    sent notification

[33mcommit 9b703e83bf5f41ab25379f3ba64259b70ad83d19[m
Merge: a365331 e703f64
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 12:26:46 2022 +0000

    Merge branch 'development' into 'main'
    
    push notification ttl
    
    See merge request abhilash-26/celebrity_school_community!37

[33mcommit e703f6403d0c1797fd24e344328fbc373f7e99a1[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 17:55:47 2022 +0530

    push notification ttl

[33mcommit a3653313d6d291b92a85412b3d4f6b0bdf032694[m
Merge: 8e90198 d107e5a
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 04:55:25 2022 +0000

    Merge branch 'development' into 'main'
    
    minor change
    
    See merge request abhilash-26/celebrity_school_community!36

[33mcommit d107e5af1ae912fada6b84c51e30660262b02c23[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 10:23:35 2022 +0530

    minor change

[33mcommit 8e90198273281b9e234e821981255667365990b8[m
Merge: f84a778 ebc0789
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 04:46:34 2022 +0000

    Merge branch 'development' into 'main'
    
    minor changes
    
    See merge request abhilash-26/celebrity_school_community!35

[33mcommit ebc0789e98e767b2bf19c9568c73f5bd8f9d86e0[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Mar 3 10:15:21 2022 +0530

    minor changes

[33mcommit f84a778a5c84a7a13dbb552c0701acb58c98e391[m
Merge: cdaaaf4 cb58e11
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 2 12:24:09 2022 +0000

    Merge branch 'development' into 'main'
    
    push notification live
    
    See merge request abhilash-26/celebrity_school_community!34

[33mcommit cb58e113f74f3cf130ab16722d2290c7c713d861[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Mar 2 17:52:30 2022 +0530

    push notification live

[33mcommit cdaaaf4c1353868f1b67cbee18165136185b7ce7[m
Merge: 7f42631 6aaf63f
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Mar 1 09:01:49 2022 +0000

    Merge branch 'development' into 'main'
    
    minor change
    
    See merge request abhilash-26/celebrity_school_community!33

[33mcommit 6aaf63f9d7707e545570a0ee555aec68b435b6cf[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Mar 1 14:30:38 2022 +0530

    minor change

[33mcommit 7f426314aabc82aa56331a8c704c574f462dddb6[m
Merge: e7497bc 7e65d14
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Mar 1 07:39:22 2022 +0000

    Merge branch 'development' into 'main'
    
    push notification
    
    See merge request abhilash-26/celebrity_school_community!32

[33mcommit 7e65d14440e2755766f3636da7e99ce2e363c324[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Mar 1 13:05:28 2022 +0530

    push notification

[33mcommit e7497bc7e2ffd03bd7ad5f7a2efcf8acf4aa613d[m
Merge: 6df6190 452553e
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Feb 24 12:50:00 2022 +0000

    Merge branch 'development' into 'main'
    
    feature flag appUpdate
    
    See merge request abhilash-26/celebrity_school_community!31

[33mcommit 452553e2d0a6c3571e54e222a8101f87f687b5ee[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Feb 24 18:18:53 2022 +0530

    feature flag appUpdate

[33mcommit 6df619048819f629915456d5eca86f25a057f6eb[m
Merge: 30c6286 7267d61
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Feb 18 14:25:25 2022 +0000

    Merge branch 'development' into 'main'
    
    delete multiple
    
    See merge request abhilash-26/celebrity_school_community!30

[33mcommit 7267d615d19128bccc9cec9b0aa76d53f092d1f0[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Feb 18 19:53:53 2022 +0530

    delete multiple

[33mcommit 30c6286d8f1ea7e3de5f4945d65c70a16829bf07[m
Merge: 9550ff0 de20cc0
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Feb 18 11:54:56 2022 +0000

    Merge branch 'development' into 'main'
    
    multiple notification
    
    See merge request abhilash-26/celebrity_school_community!29

[33mcommit de20cc053a3411c02059babf77837c909241529c[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Feb 18 17:23:02 2022 +0530

    multiple notification

[33mcommit 9550ff0fe46ee260e029ff88d897bc886143b494[m
Merge: 7661e96 d0c5488
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Feb 18 10:24:40 2022 +0000

    Merge branch 'development' into 'main'
    
    single web notification
    
    See merge request abhilash-26/celebrity_school_community!28

[33mcommit d0c5488cd0a42f97c8652d150e021616f709edc6[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Feb 18 15:43:57 2022 +0530

    single web notification

[33mcommit 7661e96c848f2a7c6b4defbfe84887dfeea14ee9[m
Merge: 2a94f8c 09234e5
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Feb 17 13:54:20 2022 +0000

    Merge branch 'development' into 'main'
    
    Development
    
    See merge request abhilash-26/celebrity_school_community!27

[33mcommit 09234e5472d4291af1c0cf51eb7b6ba6557eda20[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Feb 17 19:23:03 2022 +0530

    app config status

[33mcommit b0ad8aca5b319a865097a45952eea05fab0a74fa[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Feb 17 17:22:04 2022 +0530

    web notification crud

[33mcommit 2a94f8c15d0972672ae8e2a2cbca444a6da05526[m
Merge: a9ab7c3 8a6d81b
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Feb 11 18:53:52 2022 +0000

    Merge branch 'development' into 'main'
    
    web notification changes
    
    See merge request abhilash-26/celebrity_school_community!26

[33mcommit 8a6d81ba37b87cc4dcdfbe31217fabd044e3cdc7[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat Feb 12 00:22:54 2022 +0530

    web notification changes

[33mcommit a9ab7c3a7f85e1bff533d91170d1d83ea089c9a4[m
Merge: dfbfa27 956a023
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Feb 11 16:20:08 2022 +0000

    Merge branch 'development' into 'main'
    
    added web notification
    
    See merge request abhilash-26/celebrity_school_community!25

[33mcommit 956a023277fa31f711bf91e363aeea2b83e6243b[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Feb 11 21:49:04 2022 +0530

    added web notification

[33mcommit dfbfa2763848f369ff571bcd93c9d22a46dd6f36[m
Merge: 86bdbf3 846ada4
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Feb 2 10:17:16 2022 +0000

    Merge branch 'development' into 'main'
    
    minor change fetch last message
    
    See merge request abhilash-26/celebrity_school_community!24

[33mcommit 846ada4e7ab7e8e40c2ac34d4aff284e6395b432[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Feb 2 15:45:44 2022 +0530

    minor change fetch last message

[33mcommit 86bdbf38cd2c4252f70e9227fb766391ed572ff7[m
Merge: c60ef8b 44486c3
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Feb 1 08:38:24 2022 +0000

    Merge branch 'development' into 'main'
    
    minor change chat message format
    
    See merge request abhilash-26/celebrity_school_community!23

[33mcommit 44486c3cd2a8b66ec4ae198b3ed26d061115a169[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Feb 1 14:07:13 2022 +0530

    minor change chat message format

[33mcommit c60ef8bf2795ff67747ce785d136c5f2d26f1507[m
Merge: 60d8718 92382a2
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Feb 1 05:42:06 2022 +0000

    Merge branch 'development' into 'main'
    
    minor change chat params
    
    See merge request abhilash-26/celebrity_school_community!22

[33mcommit 92382a2132f23aace22ea5c9466feeee690d0030[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Tue Feb 1 11:10:25 2022 +0530

    minor change chat params

[33mcommit 60d87181ed13311f51b85a9e70b85e69e7e986d9[m
Merge: 60b1ad4 c352237
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Jan 31 15:02:51 2022 +0000

    Merge branch 'development' into 'main'
    
    minor change chat params
    
    See merge request abhilash-26/celebrity_school_community!21

[33mcommit c352237818a225bd410dd7554d2963f4879da297[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Jan 31 20:28:17 2022 +0530

    minor change chat params

[33mcommit 60b1ad430b223d15a54bdf527008b3cd2e4335db[m
Merge: 34a47d2 51dce92
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Jan 28 05:59:59 2022 +0000

    Merge branch 'development' into 'main'
    
    Development
    
    See merge request abhilash-26/celebrity_school_community!20

[33mcommit 51dce927218971b4622b9548354c12c09adb6a81[m
Merge: aa6d9e4 34a47d2
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Jan 28 11:28:12 2022 +0530

    Merge branch 'main' of https://gitlab.com/abhilash-26/celebrity_school_community into development

[33mcommit aa6d9e46905bbfd05d03b339dcbd211064b2f3fa[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Jan 28 11:25:56 2022 +0530

    changes in chat params

[33mcommit 34a47d26c136897adf6a3b797cd72ea95d6f1f63[m
Author: root <root@ip-10-0-2-187.ap-south-1.compute.internal>
Date:   Thu Jan 27 07:34:38 2022 +0000

    Minor fix

[33mcommit ac6eda1c70c959a8b23a93fd9c3ca4669c865f3f[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 27 13:02:42 2022 +0530

    minor fix

[33mcommit 328e3a281aca450a9aacb288b94a5a7056f01acc[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 27 13:00:27 2022 +0530

    Minor fix

[33mcommit 21c56346321ec7895133ede40c2bf2cbbbc048c4[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 27 12:56:13 2022 +0530

    Minor fix

[33mcommit 4f44cba24df726bf90ca0d5742faae3aaabfb8ff[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 27 12:09:49 2022 +0530

    minor fix

[33mcommit 95f7835882a82cf4146bd64a0dc813b475196f80[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 27 12:07:47 2022 +0530

    Add mobile number

[33mcommit 416d628ae375c4865230978e699c369317d0201f[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 27 11:58:47 2022 +0530

    Add api to get all user registerations for competition

[33mcommit 42cf6b92dcbdf7a0e310bee23c49b0928e5cd62b[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Tue Jan 25 01:48:15 2022 +0530

    Minor fix

[33mcommit 81d7085446fa9fbf4af3d3750542c4dddbc9a565[m
Merge: cdf7854 c9f7e7e
Author: negijatinn <negijatinn@gmail.com>
Date:   Tue Jan 25 01:43:57 2022 +0530

    Merge branch 'main' of https://gitlab.com/abhilash-26/celebrity_school_community

[33mcommit cdf785470216271d975130dcf6864ed80a408490[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Tue Jan 25 01:43:24 2022 +0530

    Minor fix

[33mcommit c9f7e7e53e9a749a2e4d4caa06305cc308369af3[m
Merge: ab32879 ec119c3
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jan 20 15:17:07 2022 +0000

    Merge branch 'development' into 'main'
    
    app config changes
    
    See merge request abhilash-26/celebrity_school_community!19

[33mcommit ec119c38356c23235782cd8c31c155bdd5540988[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jan 20 20:45:51 2022 +0530

    app config changes

[33mcommit ab3287916d62e72d73e57fa29cc48e78bdb5bf08[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 20 11:42:11 2022 +0530

    Change users json logic

[33mcommit 2fb5fefbe0ae1b55907f2fe00e427164388b1f07[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 20 10:47:08 2022 +0530

    Send competition name

[33mcommit 21e3f2ef375dc8b756addb71aa7c98157985531c[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 20 10:39:05 2022 +0530

    Add json route

[33mcommit 812730e5088204e4e972d144abeb85bbb75dd78f[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 20 09:20:06 2022 +0530

    Reset .env

[33mcommit c32272795dca734b54c9ad8293e2b75a46484f2c[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 20 09:18:51 2022 +0530

    Add api to find get all competition users

[33mcommit 74fcbe42233e9df95a416a31ee05c8a6e75beb1a[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 20 08:32:40 2022 +0530

    Add user update route

[33mcommit b613df47bc5aeadeff385c13b50d8ef14fe219b4[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Thu Jan 20 07:05:42 2022 +0530

    Add isPaid to competition user model

[33mcommit ca3a74de36c26d7960f0285d6d7ab4e6ffc7038b[m
Merge: 7b46baf 7d979d3
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Jan 19 15:09:53 2022 +0000

    Merge branch 'development' into 'main'
    
    userId in api
    
    See merge request abhilash-26/celebrity_school_community!18

[33mcommit 7d979d3c5d2661e3b83538ada95826c5faf7c50f[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Jan 19 20:38:16 2022 +0530

    userId in api

[33mcommit 7b46baf25bda583ef86871dfd8927d83de182d60[m
Merge: cf221f7 95d8d02
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Jan 19 08:19:08 2022 +0000

    Merge branch 'development' into 'main'
    
    user creation for competition
    
    See merge request abhilash-26/celebrity_school_community!17

[33mcommit 95d8d02ffb461669e2dbf604feb4fae5eb84eebe[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Jan 19 13:47:47 2022 +0530

    user creation for competition

[33mcommit cf221f7d8dc9bf8209c9cbf36d3991c004bd6368[m
Merge: 5304711 65ac1fd
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat Jan 8 06:52:20 2022 +0000

    Merge branch 'development' into 'main'
    
    feature flag
    
    See merge request abhilash-26/celebrity_school_community!16

[33mcommit 65ac1fd29a9a8e184f30a8557d2714c4386ef4a3[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat Jan 8 12:19:01 2022 +0530

    feature flag

[33mcommit 530471133dfd2054df2208f85a59e1ecad0b01a7[m
Merge: c8a6bd8 cf33628
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat Jan 8 06:10:31 2022 +0000

    Merge branch 'development' into 'main'
    
    feature flag
    
    See merge request abhilash-26/celebrity_school_community!15

[33mcommit cf336280df4f5ce6b4d0449dae96089b5f871f2d[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Sat Jan 8 11:37:29 2022 +0530

    feature flag

[33mcommit c8a6bd8b1051351a7fa432a10c9c60a3a9d51ce9[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Fri Jan 7 06:43:35 2022 +0530

    Minor fix

[33mcommit 67d72d0e6a40e73bb243fbc3cef124af191cd35b[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Fri Jan 7 06:05:20 2022 +0530

    Competition registration fee change

[33mcommit dbad3dc982276a4b185f58d8f77f7e5b1b37098d[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Fri Jan 7 06:03:50 2022 +0530

    Add competition payment routes

[33mcommit 0025905a4ae3b9cf2ecc40bd7cf759035927d8fc[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Fri Jan 7 00:32:24 2022 +0530

    Competition model changes

[33mcommit cb2870b418a82208ea2fd5c4ed9c39475cd6f45c[m
Author: negijatinn <negijatinn@gmail.com>
Date:   Fri Jan 7 00:20:30 2022 +0530

    Comeptition backend

[33mcommit 8c2e08bd25bfc949e773480950e12026b8efcb5f[m
Merge: 930ea42 75807f0
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jan 6 18:24:35 2022 +0000

    Merge branch 'development' into 'main'
    
    mongourl change
    
    See merge request abhilash-26/celebrity_school_community!14

[33mcommit 75807f02a60df78490588eec5eca2776f4257df0[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jan 6 23:52:28 2022 +0530

    mongourl change

[33mcommit 930ea42b0247d9622b68d4c4687c488cc537ee3a[m
Merge: 2022e5f 1845eba
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jan 6 15:49:09 2022 +0000

    Merge branch 'development' into 'main'
    
    logger changes
    
    See merge request abhilash-26/celebrity_school_community!13

[33mcommit 1845eba38a4726735ccae809a3036d43e368fc33[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jan 6 21:14:04 2022 +0530

    logger cahnges

[33mcommit 2022e5fcc219831eb36a17d2a4054fcc788eed8f[m
Merge: 28c6bb0 37182b1
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jan 6 15:19:38 2022 +0000

    Merge branch 'development' into 'main'
    
    changes inn logger
    
    See merge request abhilash-26/celebrity_school_community!12

[33mcommit 37182b1dd24771272d2a8967fa3415f23f1155cf[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Jan 6 20:48:30 2022 +0530

    changes inn logger

[33mcommit 28c6bb0632d89d149e08663418cc7b49b207f278[m
Merge: a27e01e 2b7818d
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Dec 17 15:45:21 2021 +0530

    Merge branch 'main' of https://gitlab.com/abhilash-26/celebrity_school_community into main

[33mcommit a27e01e820a0864882c910fbad4d2a839b825bb6[m
Merge: 91ab007 75d6826
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Dec 17 15:44:49 2021 +0530

    Merge branch 'development' of https://gitlab.com/abhilash-26/celebrity_school_community into main

[33mcommit 2b7818dccc6d6920871a05cc05fbe2d7afde4666[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Dec 17 10:13:45 2021 +0000

    Update Dockerfile

[33mcommit 75d682652735d115d4e1ba2080a4e32d13b7fbad[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Dec 17 15:16:12 2021 +0530

    changes test

[33mcommit 91ab00759c78f3b895180b1714a3fb9eb399e9d1[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Dec 13 10:14:21 2021 +0530

    socket changes

[33mcommit 4399326e4e11746fbf17823b0fd6e9c068244608[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Mon Dec 6 16:49:50 2021 +0530

    env config

[33mcommit 2308ec85fe4668898c8542e422ac0c9e0495abb4[m
Merge: 39ebfbb 21032f5
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Fri Dec 3 15:00:21 2021 +0000

    Merge branch 'development' into 'main'
    
    Development merge
    
    See merge request abhilash-26/celebrity_school_community!11

[33mcommit 21032f5c40e2d3bc2a0cf3f25bf285bf5056320b[m[33m ([m[1;31morigin/testing[m[33m)[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Thu Dec 2 10:18:28 2021 +0530

    db connection changes

[33mcommit f63e12d1a7470002d0c34756bb72d899268a1360[m
Author: abhilash-26 <krabhilash226@gmail.com>
Date:   Wed Dec 1 22:42:38 2021 +0530

    intial setup

[33mcommit 39ebfbbd348fea546ead726edec1985394bd21fb[m
Author: abhilash-26 <67572637+abhilash-26@users.noreply.github.com>
Date:   Wed Dec 1 09:30:41 2021 +0530

    Initial commit
